# Openhitch App (Android )

This is the current state of the [OpenHitch](https://gitlab.com/developersforfuture/wevsclimatecrisis/openhitch/openhitch) App

## Some Screenshots

![Main Menu](./files/Main.png "Screenshot Main Menu")
![Spontaneous Ride](./files/SpontaneousRide.png "Screenshot Spontaneous Ride")
![Confirmed Ride](./files/SearchRide.png "Screenshot Confirmed Ride")
