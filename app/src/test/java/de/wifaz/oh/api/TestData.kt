package de.wifaz.oh.api

import de.wifaz.oh.protocol.*
import java.math.BigDecimal
import java.util.*

open class TestData {
    companion object {
        val SECOND = 1000;
        val MINUTE = SECOND * 60
        val HOUR = MINUTE * 60
        val DAY = HOUR * 24
        val TIME0 = Date().time + DAY;  // tomorrow
        val DRIVER = User("user_idDriver", "Ferdinand", "Fahrer", "Ferdinand", null)
        val DRIVER2 = User("user_idDriver2", "Florian", "Fahrer", "Florian", null)
        val PASSENGER = User("user_idPassenger", "Moritz", "Mitfahrer", "Moritz", null)
        val PASSENGER2 = User("user_idPassenger2", "Martin", "Mitfahrer", "Martin", null)

        // some points in Berlin
        val P_HAUSBURGSTR = OHPoint.fromLngLat(13.45328, 52.52617);
        val P_TORSTR = OHPoint.fromLngLat(13.3968, 52.5287)
        val P_COTHENIUSSTR = OHPoint.fromLngLat(13.4480129, 52.5282619)
        val P_ZEHDENICKERSTR = OHPoint.fromLngLat(13.4058858, 52.5299841)

        // some driver way in Berlin
        val WAY_DRIVER = Way("idWayDriver", Way.Status.PUBLISHED, DRIVER.id, Role.DRIVER, arrayListOf(P_HAUSBURGSTR, P_TORSTR), TIME0, TIME0 + 2 * HOUR, 1, false);

        // some rider way in Berlin that matches with WAY_DRIVER. The waypoints are not directly on the route of WAY_DRIVER, but close
        val WAY_PASSENGER = Way("idWayPassenger", Way.Status.PRELIMINARY, PASSENGER.id, Role.PASSENGER, arrayListOf(
            P_COTHENIUSSTR, P_ZEHDENICKERSTR
        ), TIME0 + MINUTE, TIME0 + 2 * HOUR, 1, false);


        // Lift resulting from matching WAY_DRIVER, WAY_PASSENGER. Values are only given approximately.
        // This is not intended for comparison with real geo-information
        val LIFT = Lift(
                id = "idLift1",
                status = Lift.Status.SUGGESTED,
                passenger_way_id = WAY_PASSENGER.id,
                driver_way_id = WAY_DRIVER.id,
                pick_up_point = P_COTHENIUSSTR,
                pick_up_hint = BigDecimal(3),
                drop_off_point = P_ZEHDENICKERSTR,
                drop_off_hint = BigDecimal(6),
                rating = 3.4,
                shared_distance = 3.4,
                pickup_time = TIME0 + 5 * MINUTE,
                drop_off_time = TIME0 + 10* MINUTE,
                price = BigDecimal(1),
                currency = "EUR",
                detour_distance = 0.2,
                detour_time = 1L* MINUTE,
                sectional = false
        )
    }
}