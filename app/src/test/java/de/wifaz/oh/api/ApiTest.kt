package de.wifaz.oh.api

import de.wifaz.oh.api.*
import com.nhaarman.mockitokotlin2.*
import de.wifaz.oh.app.service.Alert
import de.wifaz.oh.app.service.ListStore
import de.wifaz.oh.protocol.*
import io.reactivex.Observable
import io.reactivex.observers.TestObserver
import okhttp3.Request
import okhttp3.RequestBody
import okhttp3.ResponseBody
import org.junit.Assert.*
import org.junit.Before
import org.junit.Test
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.util.*

fun <T> Observable<T>.testSubscribe(observer: TestObserver<T>) {
    this.subscribeOn(io.reactivex.schedulers.Schedulers.trampoline())
            .observeOn(io.reactivex.schedulers.Schedulers.trampoline())
            .subscribe(observer)
}

class TestCall<T>(val t:T) : Call<T> {
    override fun clone(): Call<T> {
        throw NotImplementedError()
    }

    override fun execute(): Response<T> {
        throw NotImplementedError()
    }

    override fun enqueue(callback: Callback<T>) {
        callback.onResponse(this, Response.success(t))
    }

    override fun isExecuted(): Boolean {
        throw NotImplementedError()
    }

    override fun cancel() {
        throw NotImplementedError()
    }

    override fun isCanceled(): Boolean {
        throw NotImplementedError()
    }

    override fun request(): Request {
        throw NotImplementedError()
    }

}

/**
 * Tests the Api against mocked server interfaces.
 */
class ApiTest : TestData() {
    /**
     * @property api  Tests perform Api-calls to simulate call from the GUI
     * @property hitchInterface  Mocked ReST-Interface called by the api. Calls to hitchInterface do not perform any actions,
     *   server actions must be simulated by the test. Test may verify, if the expected calls to the hitchInterface have been done
     * @property serverMessages Stream of Messages from the server. Tests can feed server messages to the Api
     *   by calling serverMesseges.onNext()
     * @property account Just an empty mock to initialize the api. We don't care about authentication, as we don't really
     *   talk to the server.
     */
    companion object {
        lateinit var os: Os
        lateinit var hitchInterface: HitchInterface
        lateinit var api: Api
    }

    @Before
    fun setUp() {
        os = mock()

        hitchInterface = mock()
        whenever(hitchInterface.createWay(any())).thenReturn(TestCall(Unit))
        whenever(hitchInterface.updateLiftStatus(any(),any())).thenReturn(TestCall(Unit))
        api = Api( os, hitchInterface )
    }

    @Test
    fun createWay() {
        val testObserver = TestObserver<List<Way>>()
        val wayListObservable = api.ways.wayListStream()
        wayListObservable.testSubscribe(testObserver)
        api.createWay(WAY_DRIVER)

        verify(hitchInterface).createWay(any())

        testObserver.assertValues(
                emptyList(),
                listOf(WAY_DRIVER.copy(status = Way.Status.PRELIMINARY)),
                listOf(WAY_DRIVER),
        )
    }

    @Test
    fun receiveLiftInfo() {
        api.createWay(WAY_DRIVER)
        val testObserver = TestObserver<List<LiftInfo>>();
        api.ways.liftInfoListStream(WAY_DRIVER.id)
                .testSubscribe(testObserver)
        val liftInfo = LiftInfo(LIFT, Role.DRIVER, PASSENGER, WAY_PASSENGER)
        api.digest(LiftInfoMessage(WAY_DRIVER.id, arrayListOf(liftInfo)))
        testObserver.assertValues(emptyList(),listOf(liftInfo))
    }

    @Test
    fun receiveLiftInfo_lateSubscribe() {
        api.createWay(WAY_DRIVER)
        val testObserver = TestObserver<List<LiftInfo>>()
        val liftInfo = LiftInfo(LIFT, Role.DRIVER, PASSENGER, WAY_PASSENGER)
        api.digest(LiftInfoMessage(WAY_DRIVER.id, arrayListOf(liftInfo)))
        api.ways.liftInfoListStream(WAY_DRIVER.id)
                .testSubscribe(testObserver)
        testObserver.assertValues(listOf(liftInfo))
    }

    @Test
    fun changeLiftStatus() {
        val testObserver = TestObserver<LiftInfo>()
        api.createWay(WAY_DRIVER)
        val liftInfo = LiftInfo(LIFT.copy(status=Lift.Status.REQUESTED), Role.DRIVER, PASSENGER, WAY_PASSENGER)
        api.digest(LiftInfoMessage(WAY_DRIVER.id, arrayListOf(liftInfo)))
        api.ways.liftInfoStream(WAY_DRIVER.id, LIFT.id)
                .testSubscribe(testObserver)
        api.changeLiftStatus(WAY_DRIVER.id, LIFT.id,Lift.Status.ACCEPTED)
        verify(hitchInterface).updateLiftStatus(LIFT.id,Lift.Status.ACCEPTED)
        val value = testObserver.values().last()
        assertEquals(LIFT.id,value.lift.id)
        assertEquals(Lift.Status.ACCEPTED,value.lift.status)
    }

    class LiftInfoListStore : ListStore<LiftInfo>() {
        override fun getKey(thing: LiftInfo): String {
            return thing.lift.id
        }
    }

    @Test
    fun test1() {
        val testObserver = TestObserver<LiftInfo>()
        val listStore = LiftInfoListStore()
        val liftInfo = LiftInfo(LIFT, Role.DRIVER, PASSENGER, WAY_PASSENGER)
        listStore.itemStream(LIFT.id).testSubscribe(testObserver)
        listStore.putList(arrayListOf(liftInfo))
        testObserver.assertValues(liftInfo)
    }

    @Test
    fun subscribeLiftInfo_filtered() {
        api.createWay(WAY_PASSENGER)
        val testObserver = TestObserver<List<LiftInfo>>()
        api.ways.liftInfoListStream(WAY_PASSENGER.id,EnumSet.of(Lift.Status.ACCEPTED))
                .testSubscribe(testObserver)
        val liftInfo = LiftInfo(LIFT, Role.PASSENGER, DRIVER, WAY_DRIVER)
        api.digest(LiftInfoMessage(WAY_PASSENGER.id, arrayListOf(liftInfo)))
        api.changeLiftStatus(WAY_PASSENGER.id, LIFT.id,Lift.Status.REQUESTED)
        api.digest(LiftStatusMessage(WAY_PASSENGER.id, LIFT.id, Lift.Status.ACCEPTED))
        api.digest(LiftStatusMessage(WAY_PASSENGER.id, LIFT.id, Lift.Status.DRIVER_CANCELED))
        val values = testObserver.values()
        assertEquals(3,values.size)
        assertEquals(emptyList<LiftInfo>(),values[0])
        assertEquals(listOf(liftInfo.copy(lift= LIFT.copy(status=Lift.Status.ACCEPTED))),values[1])
        assertEquals(emptyList<LiftInfo>(),values[2])
    }

    @Test
    fun requestAlert() {
        val testObserver = TestObserver<List<Alert>>()
        api.alerts.alertListStream({true})
                .testSubscribe(testObserver)
        api.createWay(WAY_DRIVER)
        val liftInfo = LiftInfo(LIFT.copy(status=Lift.Status.REQUESTED), Role.DRIVER, PASSENGER, WAY_PASSENGER)
        api.digest(LiftInfoMessage(WAY_DRIVER.id, arrayListOf(liftInfo)))
        val values = testObserver.values()
        assertEquals(2,values.size)
        assertEquals(0,values[0].size)
        assertEquals(1,values[1].size)
        assertTrue(values[1][0] is Alert.Request)
        val alert = values[1][0] as Alert.Request
        assertEquals(WAY_DRIVER.id,alert.way_id)
        assertEquals(LIFT.id,alert.lift_id)
    }

    @Test
    fun subscribeCollisions() {
        val testObserver = TestObserver<List<Way>>()
        api.createWay(WAY_DRIVER)
        val collisionListObservable = api.ways.collisionListStream(WAY_DRIVER.id)
        collisionListObservable.testSubscribe(testObserver)
        testObserver.assertValueCount(1)
        testObserver.assertValue(emptyList())
        val way2 = WAY_DRIVER.copy(id = "way2")
        api.createWay(way2)
        testObserver.assertValues(emptyList(),listOf(way2))
    }
}