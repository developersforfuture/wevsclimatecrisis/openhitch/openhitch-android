package de.wifaz.oh.app

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import de.wifaz.oh.app.R
import de.wifaz.oh.app.HitchComponent.Companion.PARAMETER_WAY_ID
import de.wifaz.oh.protocol.Role
import de.wifaz.oh.protocol.Way

class WaySettingsFragment : HitchFragment() {
    private lateinit var wayId: String

    @BindView(R.id.num_seats_label)
    lateinit var numSeatsLabel: TextView

    @BindView(R.id.num_seats_view)
    lateinit var numSeatsView: TextView

    @BindView(R.id.auto_commit_label)
    lateinit var autoCommitLabel: TextView

    @BindView(R.id.auto_commit_view)
    lateinit var autoCommitView: CheckBox


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.view_way_settings, container, false)
        ButterKnife.bind(this, view)

        return view
    }

    override fun onStart() {
        super.onStart()
        observe(api.ways.wayStream(wayId), this::update)
    }

    fun update(way: Way) {
        numSeatsLabel.text = when (way.role) {
            Role.DRIVER -> getString(R.string.free_seats)
            Role.PASSENGER -> getString(R.string.required_seats)
        }
        numSeatsView.text = Integer.toString(way.seats)


        autoCommitLabel.text = when (way.role) {
            Role.DRIVER -> getString(R.string.auto_accept)
            Role.PASSENGER -> getString(R.string.auto_request)
        }
        autoCommitView.isChecked = way.autocommit

    }

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        wayId = requireArguments().getString(PARAMETER_WAY_ID)!!
    }
}