package de.wifaz.oh.app

import android.graphics.Bitmap
import android.graphics.BitmapFactory
import de.wifaz.oh.api.Api
import de.wifaz.oh.app.service.reference_way_id
import de.wifaz.oh.protocol.LiftInfo
import io.reactivex.Observable
import io.reactivex.Observer
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import timber.log.Timber

interface HitchComponent {
    companion object {
        val PARAMETER_WAY_ID = "way_id"
        val PARAMETER_LIFT_ID = "lift_id"
    }

    val disposables: CompositeDisposable
    val api: Api

    fun <T> observe(observable: Observable<T>, action: (t: T) -> Unit) {
        val observer = AutoDisposingObserver(disposables, action)
        observable.subscribe(observer)
    }

    fun withProfilePicture(liftInfo: LiftInfo, action: (Bitmap) -> Unit) {
        observe(api.fetchPartnerPicture(liftInfo.reference_way_id(), liftInfo.partner.id)) { file ->
            val bytes = file.readBytes()
            val bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.size)
            action(bitmap)
        }
    }
}

class AutoDisposingObserver<T>(val disposables: CompositeDisposable, private val action: (t: T) -> Unit) : Observer<T> {
    override fun onSubscribe(d: Disposable) {
        disposables.add(d)
    }

    override fun onNext(t: T) {
        action(t)
    }

    override fun onError(e: Throwable) {
        Timber.e(e, "Unexpected error")
    }

    override fun onComplete() {
    }
}
