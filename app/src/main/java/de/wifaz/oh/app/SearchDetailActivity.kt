package de.wifaz.oh.app

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.TextView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import de.wifaz.oh.app.R
import com.mapbox.api.directions.v5.models.DirectionsRoute
import com.mapbox.mapboxsdk.Mapbox
import com.mapbox.mapboxsdk.maps.MapView
import com.mapbox.mapboxsdk.maps.MapboxMap
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback
import de.wifaz.oh.app.HitchComponent.Companion.PARAMETER_WAY_ID
import de.wifaz.oh.protocol.Lift
import de.wifaz.oh.protocol.LiftInfo
import de.wifaz.oh.protocol.OHPoint
import de.wifaz.oh.protocol.Way
import java.util.Date
import java.util.EnumSet

class SearchDetailActivity : HitchActivity(), OnMapReadyCallback {
    private val LOCATION_PERMISSION_REQUEST_CODE: Int = 1

    @BindView(R.id.overline)
    lateinit var overlineView: TextView

    @BindView(R.id.destination_point_view)
    lateinit var destinationPointView: TextView

    @BindView(R.id.distance_view)
    lateinit var distanceView: TextView

    @BindView(R.id.arrival_time_view)
    lateinit var arrivalTimeView: TextView

    @BindView(R.id.confirm_choice_button)
    lateinit var confirmChoiceButton: Button

    private lateinit var mapFragment: MapFragment
    private val mapView: MapView
        get() = mapFragment.mapView

    private lateinit var wayId: String
    private lateinit var way: Way

    private lateinit var start: OHPoint
    private lateinit var destination: OHPoint

    private var allLiftInfos: List<LiftInfo> = emptyList()
    private var selectedLiftInfo: LiftInfo? = null
    private var route: DirectionsRoute? = null

    private lateinit var markerColors : Array<Int>

    private var numberOfOffers = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_detail)
        initActionBar(getString(R.string.ride_search))
        Mapbox.getInstance(this.applicationContext, getString(R.string.mapbox_access_token))
        ButterKnife.bind(this)

        val extras = intent.extras
        wayId = extras!!.getString(PARAMETER_WAY_ID)!!
        way = api.ways.getWay(wayId)
        start = way.waypoints[0]
        destination = way.waypoints.last()

        mapFragment = supportFragmentManager.findFragmentById(R.id.map_fragment) as MapFragment
        mapFragment.setOnMapReadyCallback(this)

        initTextViews()

        observe(api.ways.liftInfoCountStream(wayId, EnumSet.of(Lift.Status.REQUESTED))) {
            numberOfOffers = it
            updateButton()
        }

        markerColors = arrayOf(
                ContextCompat.getColor(this, R.color.passengerMarkerColor0),
                ContextCompat.getColor(this, R.color.passengerMarkerColor1),
                ContextCompat.getColor(this, R.color.passengerMarkerColor2),
                ContextCompat.getColor(this, R.color.passengerMarkerColor3),
                ContextCompat.getColor(this, R.color.passengerMarkerColor4),
                ContextCompat.getColor(this, R.color.passengerMarkerColor5),
        )
    }

    private fun updateButton() {
        confirmChoiceButton.visibility = visibility(numberOfOffers!=0)
    }

    private fun visibility(b:Boolean) : Int {
        return if(b) {
            View.VISIBLE
        } else {
            View.GONE
        }
    }

    private fun initOfferList() {
        val offerListFragment =
                supportFragmentManager.findFragmentById(R.id.offer_list) as OfferListFragment
        offerListFragment.initialize(way, ::onSelectedItemsChanged)

    }

    private fun initTextViews() {
        val dateTimeSpec = Util.formatDateTimeSpecification(this, Date(way.start_time))
        overlineView.text = getString(R.string.your_ride_at_to_format).format(dateTimeSpec)

        destinationPointView.text = way.waypoints.last().toString()
    }

    override fun onStart() {
        super.onStart()
        LocationMemory.load(this)
    }

    override fun onStop() {
        LocationMemory.save(this)
        super.onStop()
    }

    override fun onMapReady(mapboxMap: MapboxMap) {
        api.fetchRoute(way.waypoints, {})  {route->
            mapFragment.drawRoute(route, MapFragment.DRIVER_ROUTE_SOURCE_ID)
            distanceView.text = Util.formatDistance(this, route.distance()!!)
        }
        initOfferList()
    }

    fun onChangedRouteResponse(route: DirectionsRoute) {
        this.route = route
        updateButton()

        mapFragment.drawRoute(route, MapFragment.DRIVER_ROUTE_SOURCE_ID)
        updateRouteInformation(route)
    }


    private fun updateRouteInformation(route: DirectionsRoute) {
        distanceView.text = Util.formatDistance(this, route.distance()!!)
        val arrivalTime = way.start_time + route.duration()!!.toLong() * SECOND
        arrivalTimeView.text = Util.formatTime(this, Date(arrivalTime))
    }

    private fun getMarkerColor(index:Int) : Int {
        return markerColors[index%markerColors.size]
    }

    private fun onSelectedItemsChanged(allLiftInfos: List<LiftInfo>, selectedLiftInfos: List<LiftInfo>) {
        // FIXME
    }

    private fun onRouteFailure() {
        route = null
        // FIXME clear route from map
        updateButton()
        hideKeyboard()
    }

    @OnClick(R.id.cancel_button)
    fun cancelTour() {
        api.deleteWay(wayId) {
            finish()
        }
    }


    @OnClick(R.id.confirm_choice_button)
    fun onStartButtonClick() {
        selectedLiftInfo?.let {
            api.changeLiftStatus(way.id, it.lift.id, Lift.Status.REQUESTED)
        } ?: errors.unexpected("onStartButtonClick without selectedLiftInfo")

    }

}