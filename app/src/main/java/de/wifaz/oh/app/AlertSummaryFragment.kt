package de.wifaz.oh.app

import android.content.Intent
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.core.content.ContextCompat
import de.wifaz.oh.app.R
import de.wifaz.oh.api.AlertSummaryItem
import de.wifaz.oh.app.HitchComponent.Companion.PARAMETER_WAY_ID
import de.wifaz.oh.app.service.Alert
import java.util.*

class AlertSummaryFragment : ListFragment<AlertSummaryItem>() {
    override fun onStart() {
        super.onStart()

        observe(api.alerts.alertSummaryStream()) {
            update(it)
        }

    }

    override fun onItemClick(item: AlertSummaryItem) {
        // FIXME show list,  if(item.count!=1)
        val alert = item.alert
        val intent: Intent? = when (alert) {
            is Alert.Request -> {
                var intent = Intent(activity, SearchDetailActivity::class.java)
                intent.putExtra(PARAMETER_WAY_ID, alert.way_id)
                intent
            }
            is Alert.Offer -> {
                var intent = Intent(activity, OfferDetailActivity::class.java)
                intent.putExtra(PARAMETER_WAY_ID, alert.way_id)
                intent
            }
        }

        startActivity(intent)
    }

    private fun update(alertSummaryItems: List<AlertSummaryItem>) {
        replaceItems(ArrayList<AlertSummaryItem>(alertSummaryItems))
    }

    override fun createView(container: ViewGroup, position: Int): View {
        return requireActivity().layoutInflater.inflate(R.layout.item_alert, listView, false)
    }

    override fun initItemView(view: View, item: AlertSummaryItem) {
        val textView = view as TextView
        setItemText(textView, item)
        setItemColor(textView, item)
    }

    private fun setItemColor(textView: TextView, item: AlertSummaryItem) {
        val alert = item.alert
        val colorResourceId: Int = when (alert) {
            is Alert.Request -> R.color.liftRequestAlert
            is Alert.Offer -> R.color.liftOfferAlert
        }
        textView.setBackgroundColor(ContextCompat.getColor(requireContext(), colorResourceId))
    }

    private fun setItemText(textView: TextView, item: AlertSummaryItem) {
        val alert = item.alert
        val descriptionResourceId: Int = when (alert) {
            is Alert.Request -> R.plurals.liftRequestAlertDescription
            is Alert.Offer -> R.plurals.liftOfferAlertDescription
        }
        val format = resources.getQuantityString(descriptionResourceId, item.count)
        textView.text = String.format(format, item.count)
    }
}
