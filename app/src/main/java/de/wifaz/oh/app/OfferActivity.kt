package de.wifaz.oh.app

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Button
import android.widget.Spinner
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.google.android.material.snackbar.Snackbar
import de.wifaz.oh.app.R
import de.wifaz.oh.app.HitchComponent.Companion.PARAMETER_WAY_ID
import de.wifaz.oh.protocol.OHPoint
import de.wifaz.oh.protocol.Role
import de.wifaz.oh.protocol.Way
import java.util.*

class OfferActivity : HitchActivity() {
    lateinit var waypoints: ArrayList<OHPoint>
    var duration: Long = -1

    @BindView(R.id.all)
    lateinit var rootView: View

    @BindView(R.id.max_detour_spinner)
    lateinit var maxDetourSpinner: Spinner

    @BindView(R.id.free_seats_spinner)
    lateinit var freeSeatsSpinner: Spinner

    @BindView(R.id.start_point_view)
    lateinit var startPointTextView: TextView

    @BindView(R.id.destination_point_view)
    lateinit var destinationPointTextView: TextView

    @BindView(R.id.date_button)
    lateinit var dateButton: Button
    lateinit var dateButtonControl: DateButtonControl

    @BindView(R.id.start_time_button)
    lateinit var startTimeButton: Button
    lateinit var startTimeButtonControl: TimeButtonControl

    @BindView(R.id.submit_button)
    lateinit var submitButton: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_offer)
        ButterKnife.bind(this)
        initActionBar(getString(R.string.ride_offer))

        getParameters()
        initUi()
    }

    private fun initUi() {
        initFreeSeatSpinner()
        initMaxDetourSpinner()
        initLocations()
        val now = Date()
        dateButtonControl = DateButtonControl(this,dateButton, now)
        startTimeButtonControl = TimeButtonControl(this,startTimeButton)
        startTimeButtonControl.timeOfDay = Util.getTimeOfDay(now)
    }

    private fun initLocations() {
        startPointTextView.text = waypoints[0].description
        destinationPointTextView.text = Util.getLast(waypoints).description
    }

    private fun getParameters() {
        @Suppress("UNCHECKED_CAST")
        waypoints = intent.getSerializableExtra(PARAMETER_WAYPOINTS) as ArrayList<OHPoint>
        duration = intent.getLongExtra(PARAMETER_DURATION, -1L)
        if (duration == -1L) {
            throw RuntimeException("Route duration not given")
        }
    }

    private fun initFreeSeatSpinner() {
        val items = arrayOf(1, 2, 3, 4, 5, 6, 7, 8)
        val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, items)
        freeSeatsSpinner.adapter = adapter
    }

    private fun initMaxDetourSpinner() {
        val items = arrayOf(1, 3, 5, 10, 15, 20, 30)
        val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, items)
        maxDetourSpinner.adapter = adapter
        maxDetourSpinner.setSelection(2)
    }

    @OnClick(R.id.submit_button)
    fun submitButtonClick() {
        val user = api.account.user
        if (user == null) {
            val intent = Intent(this, AccountActivity::class.java)
            startActivityForResult(intent, REQUEST_REGISTER)
        } else {
            submitButton.isEnabled = false
            val way = buildWay(user.id)
            api.createWay(way, {}) {
                val intent = Intent(this, OfferDetailActivity::class.java)
                intent.putExtra(PARAMETER_WAY_ID, way.id)
                startActivity(intent)
                finish()
            }
        }
    }

    private val REQUEST_REGISTER = 1
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(REQUEST_REGISTER, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            Snackbar.make(rootView, R.string.registration_success, Snackbar.LENGTH_LONG).show()
        }
    }

    fun buildWay(user_id: String): Way {
        val startTime = dateButtonControl.time + startTimeButtonControl.timeOfDay   // FIXME check: should not be in the past
        val maxDetourTime = ((maxDetourSpinner.selectedItem as Int) * MINUTE).toLong()

        return Way(
            id = Util.randomId(),
            status = Way.Status.PUBLISHED,
            user_id = user_id,
            role = Role.DRIVER,
            waypoints = waypoints,
            start_time = startTime,
            end_time = startTime + duration + maxDetourTime,
            seats = freeSeatsSpinner.selectedItem as Int,
            autocommit = false, // FIXME provide gui component
            max_detour_time = maxDetourTime
        )
    }

    companion object {
        val PARAMETER_DURATION = "duration"
        val PARAMETER_WAYPOINTS = "waypoints"

        private val FIRST_NAVIGATION_DIALOG_VERSION = 1
        val START_NOW = "startNow"
    }

}
