package de.wifaz.oh.app

import androidx.annotation.CallSuper
import androidx.fragment.app.Fragment
import de.wifaz.oh.api.Api
import de.wifaz.oh.api.Errors
import io.reactivex.disposables.CompositeDisposable

open class HitchFragment : Fragment(), HitchComponent {
    override val api: Api
        get() = (requireActivity().application as HitchApplication).api
    override val disposables = CompositeDisposable()

    val errors: Errors
        get() = api.errors

    @CallSuper
    override fun onStop() {
        disposables.clear()
        super.onStop()
    }
}
