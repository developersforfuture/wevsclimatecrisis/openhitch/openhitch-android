package de.wifaz.oh.app

import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import de.wifaz.oh.app.R
import de.wifaz.oh.protocol.Lift
import de.wifaz.oh.protocol.LiftInfo
import de.wifaz.oh.protocol.Way
import java.util.*
import kotlin.collections.HashSet
import kotlin.collections.List
import kotlin.collections.MutableSet
import kotlin.collections.emptyList
import kotlin.collections.filter
import kotlin.collections.sortedBy

class OfferListFragment : ListFragment<LiftInfo>() {
    private lateinit var way: Way
    private lateinit var selectedItemsListener: (allItems: List<LiftInfo>, selectedItems: List<LiftInfo>) -> Unit
    private lateinit var selectedPassengers: MutableSet<String>

    override val emptyText: String?
        get() {
            return getString(R.string.empty_offer_list_text)
        }
    
    override val maxVisibleItems = 3

    fun initialize(way: Way, selectedItemsListener: (allItems: List<LiftInfo>, selectedItems: List<LiftInfo>) -> Unit) {
        this.way = way
        this.selectedItemsListener = selectedItemsListener
        selectedPassengers = HashSet()
        val statusSet = EnumSet.of(Lift.Status.SUGGESTED,Lift.Status.REQUESTED,Lift.Status.ACCEPTED,Lift.Status.BOARDED)
        observe(api.ways.liftInfoListStream(way.id, statusSet), this::updateLifts)
    }

    private fun updateLifts(requests: List<LiftInfo>) {
        replaceItems(requests.sortedBy { it.lift.rating })
        callSelectedItemsListener()
    }


    override fun createView(container: ViewGroup, position: Int): View {
        val view = requireActivity().layoutInflater.inflate(R.layout.item_offer, listView, false)
        val colorId = if (position % 2 == 0) R.color.colorSecondaryLight else R.color.colorBackground
        view.setBackgroundColor(ContextCompat.getColor(requireContext(), colorId))
        return view
    }

    override fun initItemView(view: View, item: LiftInfo) {
        val nickname = item.partner.nick_name ?: item.partner.first_name
        setText(view, R.id.name_view, nickname)

        val imageView = view.findViewById<ImageView>(R.id.picture)
        withProfilePicture(item) {
            imageView.setImageBitmap(it)
        }

        val departureView = view.findViewById<TextView>(R.id.depature_view)
        departureView.text = Util.formatTime(requireContext(),Date(item.lift.pickup_time))

        val arrivalView = view.findViewById<TextView>(R.id.arrival_view)
        arrivalView.text = Util.formatTime(requireContext(),Date(item.lift.drop_off_time))

        val priceView = view.findViewById<TextView>(R.id.price_view)
        priceView.text = Util.formatMoney(requireContext(),item.lift.price,item.lift.currency)

        val statusView = view.findViewById<TextView>(R.id.status_view)
        val requestButton = view.findViewById<TextView>(R.id.request_button)
        when(item.lift.status) {
            Lift.Status.SUGGESTED -> {
                val rId = if (item.partner_way.autocommit) R.string.do_ride else R.string.do_request
                statusView.visibility = View.GONE
                requestButton.visibility = View.VISIBLE
                requestButton.setOnClickListener() {
                    api.changeLiftStatus(way.id,item.lift.id,Lift.Status.REQUESTED)
                }
            }
            Lift.Status.REQUESTED -> {
                requestButton.visibility = View.GONE
                statusView.visibility = View.VISIBLE
                statusView.text = getText(R.string.requested)
            }
            Lift.Status.ACCEPTED,Lift.Status.BOARDED -> {
                requestButton.visibility = View.GONE
                statusView.visibility = View.VISIBLE
                statusView.text = getText(R.string.accepted)
            }
            else -> run {errors.unexpected("Unexpected State"); R.string.invalid_value}
        }
    }

    private fun callSelectedItemsListener() {
        val allItems = items ?: emptyList()
        var selectedItems = allItems.filter {
            it.lift.id in selectedPassengers
        }
        selectedItemsListener(allItems, selectedItems)
    }

    private fun setText(view: View, resourceId: Int, text: String) {
        val textView = view.findViewById<TextView>(resourceId)
        textView.text = text

    }

    override fun onItemClick(item: LiftInfo) {
        //
    }
}