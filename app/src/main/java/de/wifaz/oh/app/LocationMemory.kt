package de.wifaz.oh.app

import android.content.Context
import androidx.preference.PreferenceManager
import com.fasterxml.jackson.core.JsonProcessingException
import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import de.wifaz.oh.app.R
import com.mapbox.turf.TurfMeasurement
import de.wifaz.oh.api.toPoint
import de.wifaz.oh.protocol.OHPoint
import timber.log.Timber
import java.io.IOException
import java.util.*

// FIXME integrate LocationMemory into api#geocodeSearch
object LocationMemory {
    private val MAX_FAVORITES = 100
    private val favorites: MutableList<OHPoint> = ArrayList()

    fun load(context: Context) {
        favorites.clear()
        val preferences = PreferenceManager.getDefaultSharedPreferences(context)
        val json = preferences.getString(context.getString(R.string.location_favorites_key), null)
        if (json != null) {
            val mapper = ObjectMapper()
            try {
                favorites.addAll(mapper.readValue(json, object : TypeReference<List<OHPoint>>() {}))
                // val collectionType = mapper.typeFactory.constructCollectionType(List<*>::class.java!!, OHPoint::class.java!!)
                // favorites = mapper.readValue<List<OHPoint>>(json, collectionType)
            } catch (e: IOException) {
                Timber.e(e, "Could not convert stored favorites to java object")
            }

        }
    }

    fun save(context: Context) {
        val mapper = ObjectMapper()
        try {
            val json = mapper.writeValueAsString(favorites)
            val editor = PreferenceManager.getDefaultSharedPreferences(context).edit()
            editor.putString(context.getString(R.string.location_favorites_key), json)
            editor.commit()
        } catch (e: JsonProcessingException) {
            // FIXME should be errors.unexpected
            Timber.e(e, "Error on writing favorites to json String")
        }

    }

    fun add(option: OHPoint) {
        for (i in favorites.indices) {
            val favorite = favorites[i]
            val p1 = option.toPoint()
            val p2 = favorite.toPoint()
            if (option.description == favorite.description && TurfMeasurement.distance(p1, p2) < 0.01) {
                favorites.removeAt(i)
                break
            }
        }
        if (favorites.size >= MAX_FAVORITES) {
            favorites.removeAt(favorites.size - 1)
        }
        favorites.add(0, option)
    }

    fun getAll(): List<OHPoint> {
        return favorites
    }

}