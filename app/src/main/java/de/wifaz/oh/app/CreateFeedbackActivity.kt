package de.wifaz.oh.app

import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.textfield.TextInputEditText
import de.wifaz.oh.protocol.Feedback
import de.wifaz.oh.protocol.User

class CreateFeedbackActivity : HitchActivity(){
    @BindView(R.id.root)
    lateinit var rootView: View

    @BindView(R.id.create_feedback_button)
    lateinit var createFeedbackButton: Button

    @BindView(R.id.edit_content)
    lateinit var editContent: TextInputEditText

    var currentUser: User? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_create_feedback)
        ButterKnife.bind(this)
        initActionBar("Leave a feedback", true)

        currentUser = api.account.user
    }

    @OnClick(R.id.create_feedback_button)
    fun onSubmitButtonClick() {
        val id = Util.randomId()
        try {
            hideKeyboard()
            val content = getNonEmptyValue(editContent)
            val newFeedback =
                Feedback(
                    id = id,
                    user_id = currentUser?.id.toString(), //ASK > isn't id already String? (.. Util.randomId())
                    content = content
                    )
            api.createFeedback(feedback = newFeedback, {} ) {
                // Display thank you message via Snackbar and then close Activity
                val snackbar = Snackbar.make(rootView, getString(R.string.thank_you_for_feedback), Snackbar.LENGTH_SHORT)
                snackbar.addCallback( object:BaseTransientBottomBar.BaseCallback<Snackbar>(){
                    override fun onDismissed(transientBottomBar: Snackbar?, event: Int) {
                        super.onDismissed(transientBottomBar, event)
                        finish()
                    }
                })
                snackbar.show()
            }
        } catch (e: AccountActivity.EmptyFieldException) {
        }

    }

    fun getNonEmptyValue(editText: EditText): String {
        val value = editText.text.toString()
        if (value == "") {
            editText.error = getString(R.string.field_mandatory)
            throw AccountActivity.EmptyFieldException()
        }
        return value
    }


}