package de.wifaz.oh.app

import android.content.Intent
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Paint
import android.graphics.PorterDuff
import android.graphics.PorterDuffXfermode
import android.view.View
import android.view.ViewGroup
import android.widget.CheckBox
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import de.wifaz.oh.app.HitchComponent.Companion.PARAMETER_LIFT_ID
import de.wifaz.oh.app.HitchComponent.Companion.PARAMETER_WAY_ID
import de.wifaz.oh.app.service.reference_way_id
import de.wifaz.oh.protocol.Lift
import de.wifaz.oh.protocol.LiftInfo
import de.wifaz.oh.protocol.Way
import java.util.*
import kotlin.collections.ArrayList
import kotlin.collections.HashSet
import kotlin.collections.List
import kotlin.collections.MutableSet
import kotlin.collections.emptyList
import kotlin.collections.filter
import kotlin.collections.sortedBy
import de.wifaz.oh.protocol.Lift.Status.*

class PassengerListFragment : ListFragment<LiftInfo>() {
    private lateinit var way: Way
    private lateinit var selectedItemsListener: (allItems: List<LiftInfo>, selectedItems: List<LiftInfo>) -> Unit
    private lateinit var selectedPassengers: MutableSet<String>
    private var manualSelectionHappened = false

    override val emptyText: String?
        get() {
            return getString(R.string.empty_passenger_list_text)
        }

    override val maxVisibleItems = 3

    fun initialize(way: Way, selectedItemsListener: (allItems: List<LiftInfo>, selectedItems: List<LiftInfo>) -> Unit) {
        this.way = way
        this.selectedItemsListener = selectedItemsListener
        selectedPassengers = HashSet()
        manualSelectionHappened = false
	// In SpontaneousRideActivity only PREVIEW should appear, in OferDeteilActivity the others
	val statusSet = EnumSet.of(Lift.Status.PREVIEW,Lift.Status.REQUESTED,Lift.Status.ACCEPTED,Lift.Status.BOARDED)
        observe(api.ways.liftInfoListStream(way.id, statusSet), this::updateLifts)
    }

    private fun updateLifts(requests: List<LiftInfo>) {
        val sortedRequests = ArrayList(requests)
        sortedRequests.sortWith( compareBy ({ statesByPrecedence.indexOf(it.lift.status) }, { -it.lift.rating }))
        if(manualSelectionHappened) {
            replaceItems(sortedRequests)
            callSelectedItemsListener()
        } else {
            val selection = proposeSelection(sortedRequests)
            selectedPassengers = HashSet(selection.map {it.lift.id})
            // sort again to see all selected Passengers on top
            sortedRequests.sortWith( compareBy (
                { statesByPrecedence.indexOf(it.lift.status) },
                { if (it.lift.id in selectedPassengers) 0 else 1},
                { -it.lift.rating }
            ))
            replaceItems(sortedRequests)
            selectedItemsListener(sortedRequests,selection)
        }
    }

    private val statesByPrecedence : List<Lift.Status> = listOf(Lift.Status.BOARDED,Lift.Status.ACCEPTED,Lift.Status.REQUESTED,Lift.Status.PREVIEW)

    /**
     * Propose a subselection of the given lifts that respects the
     * maximum detour time of ``way`` and the number of free seats
     */
    fun proposeSelection(requests: List<LiftInfo>) : List<LiftInfo> {
        val maxDetourTime = way.max_detour_time ?: Way.DEFAULT_DRIVER_MAX_DETOUR_TIME
        var totalDetourTime = 0L
        val result : MutableList<LiftInfo> = ArrayList()
        for(request in requests) {
            if( request.lift.status in setOf(ACCEPTED,BOARDED)
                || (totalDetourTime+request.lift.detour_time < maxDetourTime
                        && enoughSpace(way.seats,result+ listOf(request)))
            ) {
                    result.add(request)
                    totalDetourTime += request.lift.detour_time
            }
        }
        return result
    }

    /**
     * Determine, if we have enough space in the car to provide the given lifts
     * It is assumed that the pickup/dropoff points are headed in the order of
     * the corresponding pickup/dropoff times
     */
    private fun enoughSpace(freeSeats: Int, liftInfos: List<LiftInfo>): Boolean {
        data class Change(val time:Long, val delta:Int)

        val changes = (liftInfos.map { Change(it.lift.pickup_time,it.partner_way.seats) }
                + liftInfos.map { Change(it.lift.drop_off_time,-it.partner_way.seats) })
        var runningTotal = 0
        for (change in changes.sortedBy { it.time }) {
            runningTotal += change.delta
            if(runningTotal>freeSeats) {
                return false
            }
        }
        return true
    }

    override fun createView(container: ViewGroup, position: Int): View {
        val view = requireActivity().layoutInflater.inflate(R.layout.item_passenger, listView, false)
        val colorId = R.color.colorBackground
        view.setBackgroundColor(ContextCompat.getColor(requireContext(), colorId))
        return view
    }

    override fun initItemView(view: View, item: LiftInfo) {
        val nickname = item.partner.nick_name ?: item.partner.first_name
        setText(view, R.id.name_view, nickname)
        val detourMinutes = (item.lift.detour_time / MINUTE).toInt() + 1
        val detourTimeText =
            resources.getQuantityString(R.plurals.additional_minutes, detourMinutes, detourMinutes)
        setText(view, R.id.detour_time_view, detourTimeText)
        val locationString = item.lift.drop_off_point.description ?: "???"
        val destinationText = String.format(getString(R.string.format_passenger_destination), locationString)
        setText(view, R.id.passenger_destination_view, destinationText)

        // We should do this in #onItemClick() but that somehow doesn't work
        view.setOnClickListener {
            val intent = Intent(activity, PartnerProfileActivity::class.java)
            intent.putExtra(PARAMETER_WAY_ID,item.reference_way_id())
            intent.putExtra(PARAMETER_LIFT_ID,item.lift.id)
            startActivity(intent)
        }


        withProfilePicture(item) {bitmap->
            val r = bitmap.width/2F

            val d = 2*r
            val output = Bitmap.createBitmap(d.toInt(), d.toInt(), Bitmap.Config.ARGB_8888);

            // create initial canvas
            val c = Canvas(output)
            c.drawARGB(0, 0, 0, 0)

            // draw background marker circle
            val paint = Paint()
            paint.isAntiAlias = true
            paint.style = Paint.Style.FILL
            c.drawCircle(r, r, r, paint)

            // draw avatar image
            paint.xfermode = PorterDuffXfermode(PorterDuff.Mode.SRC_IN)
            c.drawBitmap(bitmap, 0F, 0F, paint)

            val imageView = view.findViewById<ImageView>(R.id.picture)
            imageView.setImageBitmap(output)

            // draw colored outline
            paint.style = Paint.Style.STROKE
            paint.xfermode = PorterDuffXfermode(PorterDuff.Mode.SRC_OVER)
            val index = items?.indexOf(item) ?: 0
            paint.color = ContextCompat.getColor(requireContext(), getMarkerColorResourceId(index))
            paint.setStrokeWidth(5F)

            c.drawCircle(r, r, r-3, paint)

        }

        val cbAcceptPassenger = view.findViewById<CheckBox>(R.id.cb_accept_passenger)
        val isAcceptedView = view.findViewById<TextView>(R.id.is_accepted_view)

        if (item.lift.status==Lift.Status.REQUESTED || item.lift.status==Lift.Status.PREVIEW ) {
            cbAcceptPassenger.visibility = View.VISIBLE
            isAcceptedView.visibility = View.GONE

            cbAcceptPassenger.setOnClickListener { manualSelectionHappened=true }
            cbAcceptPassenger.setOnCheckedChangeListener { _, checked->
                if (checked) {
                    selectedPassengers.add(item.lift.id)
                    view.setBackgroundColor(ContextCompat.getColor(requireContext(), R.color.colorSecondaryLight))
                } else {
                    selectedPassengers.remove(item.lift.id)
                    view.setBackgroundColor(ContextCompat.getColor(requireContext(), R.color.colorBackground))
                }
                callSelectedItemsListener()
            }
            cbAcceptPassenger.isChecked = item.lift.id in selectedPassengers
        } else {
            cbAcceptPassenger.visibility = View.GONE
            isAcceptedView.visibility = View.VISIBLE
        }
    }

    private fun callSelectedItemsListener() {
        val allItems = items ?: emptyList()
        var selectedItems = allItems.filter {
            it.lift.id in selectedPassengers
        }
        selectedItemsListener(allItems, selectedItems)
    }

    private fun setText(view: View, resourceId: Int, text: String) {
        val textView = view.findViewById<TextView>(resourceId)
        textView.text = text

    }

    override fun onItemClick(item: LiftInfo) {
        //
    }
}