package de.wifaz.oh.app

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.*
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.google.android.material.snackbar.Snackbar
import de.wifaz.oh.app.R
import de.wifaz.oh.app.HitchComponent.Companion.PARAMETER_WAY_ID
import de.wifaz.oh.protocol.OHPoint
import de.wifaz.oh.protocol.Role
import de.wifaz.oh.protocol.Way
import java.util.*
import kotlin.collections.ArrayList

//import java.util.*

class SearchRideActivity : HitchActivity() {
    private var previousStartTime: Int = -1

    @BindView(R.id.all)
    lateinit var rootView: View

    @BindView(R.id.required_seats_spinner)
    lateinit var requiredSeatsSpinner: Spinner

    @BindView(R.id.start_point_view)
    lateinit var startPointTextView: TextView

    @BindView(R.id.destination_point_view)
    lateinit var destinationPointTextView: TextView

    @BindView(R.id.date_button)
    lateinit var dateButton: Button
    lateinit var dateButtonControl: DateButtonControl

    @BindView(R.id.start_time_button)
    lateinit var startTimeButton: Button
    lateinit var startTimeButtonControl: TimeButtonControl

    @BindView(R.id.end_time_button)
    lateinit var endTimeButton: Button
    lateinit var endTimeButtonControl: TimeButtonControl

    @BindView(R.id.submit_button)
    lateinit var submitButton: Button

    @BindView(R.id.next_day_view)
    lateinit var nextDayView: TextView

    @BindView(R.id.cb_auto_request)
    lateinit var cbAutoRequest: CheckBox

    private var waypoints = ArrayList<OHPoint>()
    private var duration: Long = -1

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search_ride)
        ButterKnife.bind(this)
        initActionBar(getString(R.string.ride_search))

        getParameters()
        initUi()
    }

    private fun initUi() {
        initFreeSeatSpinner()
        initLocations()
        val now = Date()
        dateButtonControl = DateButtonControl(this,dateButton,now)
        val timeOfDay = Util.getTimeOfDay(now)
        startTimeButtonControl = TimeButtonControl(this,startTimeButton)
        startTimeButtonControl.timeOfDay = timeOfDay
        previousStartTime = timeOfDay

        val timeWindowDuration = ((duration / HOUR + 3) * HOUR).toInt()
        endTimeButtonControl = TimeButtonControl(this,endTimeButton)
        endTimeButtonControl.timeOfDay = (timeOfDay + timeWindowDuration) % DAY
        updateNextDayView()
        endTimeButtonControl.listener = {
            updateNextDayView()
        }
        startTimeButtonControl.listener = {
            dragEndTimeWithStartTime()
        }
    }

    private fun dragEndTimeWithStartTime() {
        val delta = startTimeButtonControl.timeOfDay - previousStartTime
        endTimeButtonControl.timeOfDay += delta
        previousStartTime = startTimeButtonControl.timeOfDay
    }

    private fun initLocations() {
        startPointTextView.text = waypoints[0].description
        destinationPointTextView.text = Util.getLast(waypoints).description
    }

    private fun getParameters() {
        @Suppress("UNCHECKED_CAST")
        waypoints = intent.getSerializableExtra(PARAMETER_WAYPOINTS) as ArrayList<OHPoint>
        duration = intent.getLongExtra(OfferActivity.PARAMETER_DURATION, -1L)
        if (duration == -1L) {
            throw RuntimeException("Route duration not given")
        }
    }

    private fun initFreeSeatSpinner() {
        val items = arrayOf(1, 2, 3, 4, 5, 6, 7, 8)
        val adapter = ArrayAdapter(this, android.R.layout.simple_spinner_item, items)
        requiredSeatsSpinner.adapter = adapter
    }

    fun updateNextDayView() {
        if (endTimeButtonControl.timeOfDay <= startTimeButtonControl.timeOfDay) {
            nextDayView.text = getText(R.string.next_day)
        } else {
            nextDayView.text = ""
        }
    }

    @OnClick(R.id.submit_button)
    internal fun searchRideButtonClick() {
        val user = api.account.user
        if (user == null) {
            val intent = Intent(this, AccountActivity::class.java)
            startActivityForResult(intent, REQUEST_REGISTER)
        } else {
            submitButton.isEnabled = false
            val way = buildWay(user.id)
            api.createWay(way, {}) {
                val intent = Intent(this, SearchDetailActivity::class.java)
                intent.putExtra(PARAMETER_WAY_ID, way.id)
                startActivity(intent)
                finish()
            }
        }
    }

    private val REQUEST_REGISTER = 1

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(REQUEST_REGISTER, resultCode, data)
        if (resultCode == Activity.RESULT_OK) {
            Snackbar.make(rootView, R.string.registration_success, Snackbar.LENGTH_LONG).show()
        }
    }


    private fun buildWay(user_id: String): Way {
        val requiredSeats = requiredSeatsSpinner.selectedItem as Int
        val way_id = Util.randomId()
        val startTime = dateButtonControl.time + startTimeButtonControl.timeOfDay // FIXME check: should not be in the past
        var endTime = dateButtonControl.time + endTimeButtonControl.timeOfDay
        if (endTime < startTime) {
            endTime += DAY
        }


        val way = Way(
            id = way_id,
            status = Way.Status.PUBLISHED,
            user_id = user_id,
            role = Role.PASSENGER,
            waypoints = waypoints,
            start_time = startTime,
            end_time = endTime,
            seats = requiredSeats,
            autocommit = cbAutoRequest.isChecked,
            sectional_match = false
        )
        return way
    }

    companion object {
        val PARAMETER_WAYPOINTS = "waypoints"
        val PARAMETER_DURATION = "duration"
    }
}
