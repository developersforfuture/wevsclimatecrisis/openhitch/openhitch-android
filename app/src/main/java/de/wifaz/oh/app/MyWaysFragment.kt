package de.wifaz.oh.app

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView

import de.wifaz.oh.app.R
import de.wifaz.oh.app.HitchComponent.Companion.PARAMETER_WAY_ID
import de.wifaz.oh.protocol.Lift
import de.wifaz.oh.protocol.Role

import de.wifaz.oh.protocol.Role.DRIVER
import de.wifaz.oh.protocol.Role.PASSENGER
import de.wifaz.oh.protocol.Way
import java.util.Date

class MyWaysFragment : ListFragment<Way>() {

    override fun onItemClick(item: Way) {
        val activityClass = when(item.role) {
            PASSENGER->SearchDetailActivity::class.java
            DRIVER->OfferDetailActivity::class.java
        }
        var intent = Intent(activity, activityClass)
        intent.putExtra(PARAMETER_WAY_ID, item.id)
        startActivity(intent)
        requireActivity().finish()
        Intent()
    }

    private fun updateItems(wayContexts: List<Way>) {
        replaceItems(wayContexts.sortedBy { it.start_time })
    }

    override fun createView(container: ViewGroup, position: Int): View {
        return requireActivity().layoutInflater.inflate(R.layout.item_way, container, false)
    }

    override fun onStart() {
        super.onStart()
        observe(api.ways.wayListStream { it.status == Way.Status.PUBLISHED }, this::updateItems)
    }

    override fun initItemView(view: View, item: Way) {
        val wayView = view.findViewById<View>(R.id.way_view) as WayView
        renderItemHeader(view, item)

        wayView.setWay(item)

        observe(api.ways.liftInfoCountMapStream(item.id)) {
            when (item.role) {
                DRIVER -> setPartnerInfosDriver(wayView,it)
                PASSENGER -> setPartnerInfosPassenger(wayView,it)
            }
        }

    }

    private fun renderItemHeader(view: View, item: Way) {
        val formatResource = when (item.role) {
            DRIVER -> R.string.way_header_format_driver
            PASSENGER -> R.string.way_header_format_passenger
        }
        val format = getString(formatResource)
        val dateSpec = Util.formatDateTimeSpecification(requireContext(), Date(item.start_time))
        val text = format.format(dateSpec)
        val headerView = view.findViewById<TextView>(R.id.header)
        headerView.setText(text)
    }

    private fun setPartnerInfosDriver(wayView:WayView, m: Map<Lift.Status, Int?>) {
        // FIXME use getQuantityString for pluralisation
        val passengers = (m.get(Lift.Status.ACCEPTED) ?: 0) + (m.get(Lift.Status.BOARDED) ?: 0)
        val requests = m.get(Lift.Status.REQUESTED) ?: 0
        val s = String.format(getString(R.string.passengers_requests_format), passengers, requests)
        wayView.partnerInfo.text = s
    }

    private fun setPartnerInfosPassenger(wayView:WayView, m: Map<Lift.Status, Int?>) {
        // FIXME use getQuantityString for pluralisation
        val accepted = m.get(Lift.Status.ACCEPTED) ?: 0 + (m.get(Lift.Status.BOARDED) ?: 0)
        val requested = m.get(Lift.Status.REQUESTED) ?: 0
        val offered = m.get(Lift.Status.SUGGESTED) ?: 0
        val s: String = if (accepted > 0) {
            getString(R.string.lift_agreed) + (if (offered > 0) {
                ", " + String.format(getString(R.string.more_offers_available), offered)
            } else "")
        } else if (requested > 0) {
            getString(R.string.lift_requested) + (if (offered > 0) {
                ", " + String.format(getString(R.string.more_offers_available), offered)
            } else "")
        } else {
            String.format(getString(R.string.offers_available), offered)
        }
        wayView.partnerInfo.text = s
    }


}
