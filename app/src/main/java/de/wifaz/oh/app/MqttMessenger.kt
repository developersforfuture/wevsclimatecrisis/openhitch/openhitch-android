package de.wifaz.oh.app

import android.content.Context
import com.fasterxml.jackson.core.JsonParseException
import com.fasterxml.jackson.databind.ObjectMapper
import de.wifaz.oh.app.R
import de.wifaz.oh.api.Account
import de.wifaz.oh.api.Errors
import de.wifaz.oh.api.Messenger
import de.wifaz.oh.protocol.HitchMessage
import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject
import org.eclipse.paho.android.service.MqttAndroidClient
import org.eclipse.paho.client.mqttv3.*
import timber.log.Timber

class MqttMessenger(private val context: Context, private val errors: Errors) : MqttCallback, Messenger {
    private val mqttServerURI = context.getString(R.string.mqtt_server_uri)
    private var mqttAndroidClient: MqttAndroidClient? = null
    private val messageSubject: PublishSubject<HitchMessage> = PublishSubject.create()
    private lateinit var account: Account

    override fun messageStream(account: Account): Observable<HitchMessage> {
        this.account = account
        account.isAuthenticatedStream().subscribe(this::onAuthenticationStateChange)
        return messageSubject
    }

    private fun createMqttClient(): MqttAndroidClient {
        // FIXME use user id?
        val clientId = MqttClient.generateClientId()

        // FIXME configure IP
        // FIXME use MqttClientPersistence as a third parameter?
        val client = MqttAndroidClient(context, mqttServerURI, clientId)
        client.setCallback(this)
        return client
    }

    private fun mqttConnect(client: MqttAndroidClient, topic: String) {
        // For using the Eclipse Paho Android Service cf https://github.com/eclipse/paho.mqtt.android/blob/develop/BasicExample/src/main/java/paho/mqtt/java/example/PahoExampleActivity.java

        if (client.isConnected) {
            return
        }


        try {
            val mqttConnectOptions = MqttConnectOptions()
            mqttConnectOptions.isAutomaticReconnect = true
            mqttConnectOptions.isCleanSession = true
            client.connect(mqttConnectOptions, null, object : IMqttActionListener {
                override fun onSuccess(asyncActionToken: IMqttToken) {
                    val options = DisconnectedBufferOptions()
                    options.isBufferEnabled = true
                    options.bufferSize = 100
                    options.isPersistBuffer = true
                    options.isDeleteOldestMessages = false
                    client.setBufferOpts(options)
                    mqttSubscribe(client, topic)
                }

                override fun onFailure(asyncActionToken: IMqttToken, exception: Throwable) {
                    // FIXME separate errors by cause: authentication and connectivity errors. Throw really unexpected errors
                    // FIXME try to reconnect?
                    errors.unexpected("Failed to connect to Mqtt-Server", "mqttServerURI: $mqttServerURI", exception)
                }
            })
        } catch (e: MqttException) {
            errors.unexpected("Crashed on attempt to connect to Mqtt-Server", "mqttServerURI: $mqttServerURI", e)
        }

    }

    private fun mqttSubscribe(client: MqttAndroidClient, topic: String) {
        try {
            client.subscribe(topic, 1, null, object : IMqttActionListener {
                override fun onSuccess(asyncActionToken: IMqttToken) {
                    Timber.d("Mqtt subscribed topic %s", topic)
                }

                override fun onFailure(asyncActionToken: IMqttToken, exception: Throwable) {
                    // FIXME separate errors by cause, see above
                    errors.unexpected("MQTT-subscription failed", null, exception)
                }
            })
        } catch (e: MqttException) {
            errors.unexpected("MQTT-subscription crashed", null, e)
        }
    }

    @Synchronized
    override fun messageArrived(topic: String, message: MqttMessage) {
        try {
            Timber.i("MQTT message arrived, topic=%s, message=%s", topic, message)
            val json = message.toString()
            val mapper = ObjectMapper()
            // FIXME enable this in publish build variant
            // mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
            val messageObject: HitchMessage
            try {
                messageObject = mapper.readValue<HitchMessage>(json, HitchMessage::class.java)
                messageSubject.onNext(messageObject)
            } catch (e: JsonParseException) {
                // FIXME separate by cause, see above
                errors.unexpected("MQTT message json error", "message: $json", e)
                return
            }

        } catch (e: Exception) {
            errors.unexpected("messageArrived crashed", null, e)
        }

    }

    override fun connectionLost(cause: Throwable) {
        // FIXME
    }

    override fun deliveryComplete(token: IMqttDeliveryToken) {
        // Nothing to do as we do not send messages
    }

    @Synchronized
    private fun onAuthenticationStateChange(isAuthenticated: Boolean) {
        if (isAuthenticated) {
            if (mqttAndroidClient == null) {
                val client = createMqttClient()
                val topic = account.user!!.id
                Timber.d("Connecting to MQTT topic %s", topic)
                mqttConnect(client, topic)
                mqttAndroidClient = client
            }
        } else {
            mqttAndroidClient?.close()
            mqttAndroidClient = null
        }
    }
}