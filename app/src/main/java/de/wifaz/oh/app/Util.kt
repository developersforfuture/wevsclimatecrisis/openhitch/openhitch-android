package de.wifaz.oh.app

import android.content.Context
import android.text.format.DateUtils
import de.wifaz.oh.app.R
import com.mapbox.services.android.navigation.v5.utils.LocaleUtils
import de.wifaz.oh.app.Preferences.getUnitType
import de.wifaz.oh.protocol.Lift
import de.wifaz.oh.protocol.Role
import java.math.BigDecimal
import java.security.SecureRandom
import java.text.DateFormat
import java.text.NumberFormat
import java.text.SimpleDateFormat
import java.util.*

object Util {
    private val base64chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789-_"
    private val randomIdLength = 27 //

    internal var localeUtils = LocaleUtils()

    private val random = SecureRandom()

    /**
     * Create random id as a string
     *
     * @return random String of 27 base64 characters (=entropy 162 bit)
     */
    fun randomId(): String {
        val sb = StringBuilder(randomIdLength)
        for (i in 0 until randomIdLength)
            sb.append(base64chars[random.nextInt(base64chars.length)])
        return sb.toString()
    }

    fun isEmpty(s: String?): Boolean {
        return s == null || s.length == 0
    }

    fun formatDate(context: Context, date: Date): String {
        if (DateUtils.isToday(date.time)) {
            return context.getString(R.string.today)
        } else if (isTomorrow(date.time)) {
            return context.getString(R.string.tomorrow)
        } else {
            val df1 = SimpleDateFormat("EE, ")  // day of week
            val df2 = DateFormat.getDateInstance(DateFormat.SHORT, Locale.getDefault())
            return df1.format(date) + df2.format(date)
        }
    }

    private fun isTomorrow(time: Long): Boolean {
        val then = GregorianCalendar()
        then.timeInMillis = time
        val tomorrow: Calendar = GregorianCalendar()
        tomorrow.add(Calendar.DAY_OF_MONTH, 1)
        return then.get(Calendar.YEAR) == tomorrow.get(Calendar.YEAR) && then.get(Calendar.DAY_OF_YEAR) == tomorrow.get(
            Calendar.DAY_OF_YEAR
        )
    }

    fun truncateDate(date: Date): Date {
        val cal: Calendar = GregorianCalendar()
        cal.time = date
        cal.set(Calendar.HOUR_OF_DAY, 0)
        cal.set(Calendar.MINUTE, 0)
        cal.set(Calendar.SECOND, 0)
        cal.set(Calendar.MILLISECOND, 0)
        return cal.time
    }

    fun getTimeOfDay(date: Date): Int {
        return (date.time - truncateDate(date).time).toInt()
    }

    fun formatTime(context: Context, date: Date): String {
        val tf = android.text.format.DateFormat.getTimeFormat(context)
        return tf.format(date)
    }

    fun formatTime(context: Context, timeOfDay: Int): String {
        val time = truncateDate(Date()).time + timeOfDay
        return formatTime(context, Date(time))
    }


    fun formatDateTime(context: Context, date: Date): String {
        return formatDate(context, date) + " " + formatTime(context, getTimeOfDay(date))
    }

    fun formatDateTimeSpecification(context: Context, date:Date) : String {
        val timeString = formatTime(context, getTimeOfDay(date))
        if (DateUtils.isToday(date.time)) {
            val format =  context.getString(R.string.time_specification_format_today)
            return format.format(timeString)
        } else if (isTomorrow(date.time)) {
            val format =  context.getString(R.string.time_specification_format_tomorrow)
            return format.format(timeString)
        } else {
            val dateString = DateFormat.getDateInstance(DateFormat.SHORT, Locale.getDefault()).format(date)
            val format =  context.getString(R.string.time_specification_format_general)
            return format.format(dateString,timeString)
        }
    }

    fun formatMoney(context: Context, price: BigDecimal, currencyCode: String): String {
        val currency = Currency.getInstance(currencyCode)
        val numberFormat = NumberFormat.getCurrencyInstance(Preferences.getLocale(context))
        numberFormat.currency = currency
        return numberFormat.format(price)
    }

    fun <T> getLast(list: List<T>): T {
        return list[list.size - 1]
    }

    fun formatDistance(context: Context, distanceInMeters: Double): String {
        when (getUnitType(context)) {
            "imperial" -> {
                val miles = distanceInMeters / 1609.344
                return String.format("%.1f mi", miles)
            }
            else -> {
                return String.format("%.1f km", distanceInMeters / 1000)
            }
        }
    }
}

fun liftTitle(context: Context, status: Lift.Status, role: Role): String {
    var resId: Int = when (status) {
        Lift.Status.SUGGESTED -> R.string.ride_offer
        Lift.Status.REQUESTED -> R.string.ride_request
        Lift.Status.ACCEPTED -> when (role) {
            Role.DRIVER -> R.string.passenger
            Role.PASSENGER -> R.string.agreed_lift
        }
        Lift.Status.BOARDED -> R.string.started_ride
        Lift.Status.FINISHED -> R.string.finished_ride
        Lift.Status.DRIVER_CANCELED -> R.string.lift_title_driver_canceled
        Lift.Status.PASSENGER_CANCELED -> R.string.lift_title_passenger_canceled
        Lift.Status.REJECTED -> R.string.lift_title_rejected
        Lift.Status.WITHDRAWN -> R.string.lift_title_withdrawn
        Lift.Status.PREVIEW -> R.string.lift_title_preview
    }
    return context.getText(resId) as String
}

