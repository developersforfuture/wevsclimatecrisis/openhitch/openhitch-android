package de.wifaz.oh.app

import android.os.Bundle
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import de.wifaz.oh.app.HitchComponent.Companion.PARAMETER_LIFT_ID
import de.wifaz.oh.app.HitchComponent.Companion.PARAMETER_WAY_ID
import de.wifaz.oh.protocol.LiftInfo
import java.util.*

class PartnerProfileActivity : HitchActivity(){
    lateinit var liftInfo : LiftInfo

    @BindView(R.id.picture)
    lateinit var picture: ImageView

    @BindView(R.id.phone_view)
    lateinit var phoneView: TextView

    @BindView(R.id.name_view)
    lateinit var nameView: TextView

    @BindView(R.id.pickup_point_view)
    lateinit var pickupPointView: TextView

    @BindView(R.id.pickup_time_view)
    lateinit var pickupTimeView: TextView

    @BindView(R.id.dropoff_point_view)
    lateinit var dropoffPointView: TextView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val liftId = intent.getStringExtra(PARAMETER_LIFT_ID)
        val wayId = intent.getStringExtra(PARAMETER_WAY_ID)

        liftInfo = api.ways.getLiftInfo(way_id = wayId, lift_id = liftId)
        setContentView(R.layout.activity_partner_profile)
        ButterKnife.bind(this)
        initActionBar(getString(R.string.partner_profile))

        val partner = liftInfo.partner
        nameView.text = "${partner.first_name} ${partner.last_name}"
        val phone = partner.phone
        phoneView.text =  phone?.replace("[^+0-9]".toRegex(), "")

        pickupPointView.text = liftInfo.lift.pick_up_point.toString()
        pickupTimeView.text = Util.formatDateTimeSpecification(this, Date(liftInfo.lift.pickup_time))
        dropoffPointView.text = liftInfo.lift.drop_off_point.toString()

        withProfilePicture(liftInfo) {
            picture.setImageBitmap(it)
        }
    }



    }