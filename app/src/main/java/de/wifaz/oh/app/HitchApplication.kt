package de.wifaz.oh.app

import android.content.Intent
import androidx.multidex.MultiDexApplication
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.ObjectMapper
import de.wifaz.oh.app.BuildConfig
import de.wifaz.oh.app.R
import de.wifaz.oh.api.Api
import de.wifaz.oh.api.Errors
import de.wifaz.oh.api.HitchApiReferenceException
import de.wifaz.oh.api.Messenger
import de.wifaz.oh.protocol.HitchInterface
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.jackson.JacksonConverterFactory
import timber.log.Timber
import java.io.IOException

class HitchApplication : MultiDexApplication() {
    lateinit var api: Api
    lateinit var messenger: Messenger

    override fun onCreate() {
        super.onCreate()
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
        createApi()
    }

    private fun createApi() {
        val os = AndroidOs(this)
        val hitchInterface = createRetrofitClient()
        api = Api(os, hitchInterface)
        api.settings.locale = Preferences.getLocale(this)
        api.settings.unitType = Preferences.getUnitType(this) ?: "Metric"
        api.settings.navigateApiUrl = getString(R.string.navigate_api_url)
        api.sync()
        api.errors.errorStream().subscribe { onError(it) }
        subscribeMessages()

    }

    private fun onError(error: Errors.Error) {
        Timber.e("Error type ${error.type.name}: ${error.message}, info=${error.info}")
        error.throwable?.let { Timber.e(it) }

        if (error.type == Errors.Type.AUTHENTICATION) {
            api.account.setUser(null, null, null)
            val intent = Intent(applicationContext, RegistrationActivity::class.java)
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP or Intent.FLAG_ACTIVITY_NEW_TASK)
            intent.putExtra(RegistrationActivity.PARAMETER_IS_AUTHENTICATION_ERROR, true)
            startActivity(intent)
        }
    }


    private fun subscribeMessages() {
        messenger = MqttMessenger(this, api.errors)
        messenger.messageStream(api.account).subscribe {
            try {
                api.digest(it)
            } catch (exc: HitchApiReferenceException) {
                api.errors.unexpected(
                    "Could not handle message from server due to invalid reference",
                    "message: $it",
                    exc
                )
            }
        }
    }

    private fun createRetrofitClient(): HitchInterface {
        val hitchApiUrl: String = getString(R.string.hitch_api_url)
        val objectMapper = ObjectMapper()
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL)
        val httpClient = OkHttpClient.Builder()
            .addInterceptor(AuthenticationInterceptor())
            .build()
        val retrofit = Retrofit.Builder()
            .baseUrl(hitchApiUrl)
            .addConverterFactory(JacksonConverterFactory.create(objectMapper))
            .client(httpClient)
            .build()
        return retrofit.create<HitchInterface>(HitchInterface::class.java)
    }

    internal inner class AuthenticationInterceptor : Interceptor {
        @Throws(IOException::class)
        override fun intercept(chain: Interceptor.Chain): okhttp3.Response {
            val original = chain.request()
            val request = original.newBuilder()
                .addHeader("Authorization", "Bearer " + api.account.token)
                .build()
            return chain.proceed(request)
        }
    }

}