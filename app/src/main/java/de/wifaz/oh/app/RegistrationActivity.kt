package de.wifaz.oh.app

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import de.wifaz.oh.app.BuildConfig
import de.wifaz.oh.app.R
import de.wifaz.oh.api.Api

class RegistrationActivity : HitchActivity() {
    companion object {
        val PARAMETER_IS_AUTHENTICATION_ERROR = "isAuthenticationError"
    }

    private val REQUEST_ACCOUNT = 1
    private val REQUEST_PROFILE_PICTURE = 2

    @BindView(R.id.about_registration)
    lateinit var registrationStateTextView: TextView

    @BindView(R.id.apology_registraton_error)
    lateinit var apologyTextView: TextView

    @BindView(R.id.register_button)
    lateinit var registerButton: Button

    @BindView(R.id.profile_picture_button)
    lateinit var profilePictureButton: Button

    @BindView(R.id.goto_main_button)
    lateinit var gotoMainButton: Button

    @BindView(R.id.persona_button)
    lateinit var personaButton: Button

    lateinit var faqEntries: List<FaqEntry>

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_registration)
        ButterKnife.bind(this)
        if (BuildConfig.DEBUG) {
            personaButton.visibility = View.VISIBLE
        }
        prepareApology()
        prepareFaq()
    }

    private fun prepareApology() {
        if (intent.getBooleanExtra(PARAMETER_IS_AUTHENTICATION_ERROR, false)) {
            apologyTextView.visibility = View.VISIBLE
        } else {
            apologyTextView.visibility = View.GONE
        }

    }

    private fun prepareFaq() {
        faqEntries = listOf(
            FaqEntry(R.id.question_why_registration, R.id.answer_why_registration),
            FaqEntry(R.id.question_which_data, R.id.answer_which_data),
            FaqEntry(R.id.question_about, R.id.answer_about),
        )
        faqEntries.forEachIndexed { i, faq ->
            findViewById<TextView>(faq.questionResourceId).setOnClickListener({ showAnswer(i) })
        }
    }

    private fun showAnswer(index: Int) {
        faqEntries.forEachIndexed { i, faq ->
            val visibility = if (i == index) View.VISIBLE else View.GONE
            findViewById<TextView>(faq.answerResourceId).visibility = visibility
        }
    }

    private fun hideFaq() {
        faqEntries.forEach { faq ->
            findViewById<TextView>(faq.questionResourceId).visibility = View.GONE
            findViewById<TextView>(faq.answerResourceId).visibility = View.GONE
        }
    }


    override fun onStart() {
        super.onStart()
        checkRegistration(api)
    }

    private fun checkRegistration(api: Api) {
        registerButton.visibility = View.GONE
        profilePictureButton.visibility = View.GONE
        gotoMainButton.visibility = View.GONE
        registrationStateTextView.visibility = View.GONE
        if (api.account.isAutheticated()) {
            if (false != api.account.registrationState?.has_profile_picture) {
                registrationStateTextView.visibility = View.VISIBLE
                registrationStateTextView.text = getText(R.string.registration_success)
                hideFaq()
                gotoMainButton.visibility = View.VISIBLE
            } else {
                registrationStateTextView.visibility = View.VISIBLE
                registrationStateTextView.text = getText(R.string.profile_picture_missing)
                profilePictureButton.visibility = View.VISIBLE
            }
        } else {
            registerButton.visibility = View.VISIBLE
            profilePictureButton.visibility = View.GONE
        }

    }

    @OnClick(R.id.register_button)
    fun registerButtonClick() {
        val intent = Intent(this, AccountActivity::class.java)
        startActivityForResult(intent, REQUEST_ACCOUNT)
    }

    @OnClick(R.id.profile_picture_button)
    fun profilePictureButtonClick() {
        val intent = Intent(this, ProfilePictureActivity::class.java)
        intent.putExtra(ProfilePictureActivity.PARAMETER_MODE, ProfilePictureActivity.MODE_REGISTRATION)
        startActivityForResult(intent, REQUEST_PROFILE_PICTURE)
    }

    @OnClick(R.id.goto_main_button)
    fun gotoMain() {
        startActivity(Intent(this, MainActivity::class.java))
        finish()
    }


    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            REQUEST_ACCOUNT -> {
                if (resultCode == RESULT_OK) {
                    val intent = Intent(this, ProfilePictureActivity::class.java)
                    startActivityForResult(intent, REQUEST_PROFILE_PICTURE)
                }
            }
            REQUEST_PROFILE_PICTURE -> {
                // ignore, work is done in #checkRegistration
            }
        }
    }

    @OnClick(R.id.persona_button)
    fun onPersonaButtonClick() {
        showPersonaChooserDialog(this)
    }
}

data class FaqEntry(
    val questionResourceId: Int,
    val answerResourceId: Int,
)