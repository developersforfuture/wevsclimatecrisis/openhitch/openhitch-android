package de.wifaz.oh.app

import android.content.Context
import android.os.Bundle
import android.view.View
import android.view.View.GONE
import android.view.View.VISIBLE
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import android.widget.Toast
import androidx.core.content.ContextCompat
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.google.android.material.snackbar.Snackbar
import de.wifaz.oh.app.R
import com.mapbox.api.directions.v5.models.DirectionsRoute
import com.mapbox.mapboxsdk.Mapbox
import com.mapbox.mapboxsdk.geometry.LatLng
import com.mapbox.mapboxsdk.maps.MapView
import com.mapbox.mapboxsdk.maps.MapboxMap
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback
import com.mapbox.services.android.navigation.ui.v5.route.NavigationMapRoute
import de.wifaz.oh.protocol.Lift
import de.wifaz.oh.protocol.LiftInfo
import de.wifaz.oh.protocol.OHPoint
import de.wifaz.oh.protocol.Role
import de.wifaz.oh.protocol.Way
import java.util.Date

class SpontaneousRideActivity : HitchActivity(), OnMapReadyCallback, MapboxMap.OnMapClickListener,
    MapboxMap.OnMapLongClickListener, SearchLocationListener {
    private val LOCATION_PERMISSION_REQUEST_CODE: Int = 1

    @BindView(R.id.search_frame)
    lateinit var searchFrame: View

    @BindView(R.id.bottom_part)
    lateinit var bottomPart: View

    @BindView(R.id.destination_point_view)
    lateinit var destinationPointView: TextView

    @BindView(R.id.distance_view)
    lateinit var distanceView: TextView

    @BindView(R.id.arrival_time_view)
    lateinit var arrivalTimeView: TextView

    @BindView(R.id.start_button)
    lateinit var startButton: TextView

    @BindView(R.id.new_search_button)
    lateinit var newSearchButton: TextView

    private lateinit var searchFragment: SearchLocationFragment

    private var isAutocompletionActive: Boolean = true

    private lateinit var mapFragment: MapFragment
    private lateinit var mapRoute: NavigationMapRoute
    private val mapView: MapView
        get() = mapFragment.mapView

    private var start: OHPoint? = null
    private var way: Way? = null
    private var selectedLiftInfos: List<LiftInfo>? = null
    private var route: DirectionsRoute? = null

    private val destination : OHPoint?
        get() = searchFragment.waypoint

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_spontaneous_ride)
        initActionBar(getString(R.string.offer_spontaneous_ride))
        Mapbox.getInstance(this.applicationContext, getString(R.string.mapbox_access_token))
        ButterKnife.bind(this)

        mapFragment = supportFragmentManager.findFragmentById(R.id.map_fragment) as MapFragment
        mapFragment.setOnMapReadyCallback(this)
    }

    override fun onStart() {
        super.onStart()
        LocationMemory.load(this)
    }

    override fun onStop() {
        LocationMemory.save(this)
        super.onStop()
    }

    override fun onDestroy() {
        val way_ = way
        if (way_ != null && way_.status == Way.Status.RESEARCH) {
            api.deleteWay(way_.id)
        }
        super.onDestroy()
    }

    override fun onMapReady(mapboxMap: MapboxMap) {
        mapboxMap.addOnMapClickListener(this)
        mapboxMap.addOnMapLongClickListener(this)
        mapRoute = NavigationMapRoute(mapView, mapboxMap)
        searchFragment = supportFragmentManager.findFragmentById(R.id.search_fragment) as SearchLocationFragment
        searchFragment.init(mapFragment, this)
        searchFragment.setLabel(getString(R.string.search_destination_or_click))
        searchFragment.focus()
    }


    override fun onMapClick(point: LatLng) {
        onAnyMapClick(point)
    }

    override fun onMapLongClick(point: LatLng) {
        onAnyMapClick(point)
    }

    private fun onAnyMapClick(point: LatLng) {
        val ohPoint = point.toOHPoint()
        searchFragment.waypoint = ohPoint
        updateRoute()
        searchFragment.startReverseSearch(ohPoint) {
            destinationPointView.text = destination?.description ?: "???"
        }
    }

    override fun updateRoute() {
        hideKeyboard()
        val startPoint = mapFragment.lastKnownLocation?.toOHPoint()
        start = startPoint
        val destinationPoint = destination
        if (startPoint == null) {
            Snackbar.make(mapView, R.string.current_location_unknown, Snackbar.LENGTH_LONG).show()
            return
        } else if (destinationPoint == null) {
            throw HitchAppError("Destination point not set")
        } else {
            api.fetchRoute(listOf(startPoint, destinationPoint), ::onRouteFailure, ::onRouteResponse)
        }
    }

    fun onRouteResponse(route: DirectionsRoute) {
        this.route = route
        startButton.isEnabled = true
        isAutocompletionActive = false
        searchFrame.visibility = GONE
        mapFragment.drawRoute(route, MapFragment.DRIVER_ROUTE_SOURCE_ID)
        doCreateWay(route)
        destinationPointView.text = destination?.description ?: "???"
        updateRouteInformation(route)
        bottomPart.visibility = VISIBLE
    }

    fun onChangedRouteResponse(route: DirectionsRoute) {
        this.route = route
        startButton.isEnabled = true
        mapFragment.drawRoute(route, MapFragment.DRIVER_ROUTE_SOURCE_ID)
        updateRouteInformation(route)
    }


    private fun doCreateWay(route: DirectionsRoute) {
        val user = api.account.user!! // FIXME register if needed
        val way = buildWay(user.id, route)
        this.way?.let {
            api.deleteWay(it.id)
        }
        this.way = way
        api.createWay(way, {}) {
            val passengerListFragment =
                supportFragmentManager.findFragmentById(R.id.passenger_list) as PassengerListFragment
            passengerListFragment.initialize(way, ::onSelectedItemsChanged)
        }
    }

    private fun updateRouteInformation(route: DirectionsRoute) {
        distanceView.text = Util.formatDistance(this, route.distance()!!)
        val now: Long = Date().time
        val arrivalTime = now + route.duration()!!.toLong() * SECOND
        arrivalTimeView.text = Util.formatTime(this, Date(arrivalTime))
    }


    private fun buildWay(user_id: String, route: DirectionsRoute): Way {
        val waypoints = ArrayList(listOf(start!!, destination!!))  // FIXME can we play this null-safe?
        val startTime = Date().time
        val maxDetourTime = (10 * MINUTE).toLong()  // FIXME make configurable
        val duration = route.duration()!!.toLong() * SECOND

        return Way(
                id = Util.randomId(),
                status = Way.Status.RESEARCH,
                user_id = user_id,
                role = Role.DRIVER,
                waypoints = waypoints,
                start_time = startTime,
                end_time = startTime + duration + maxDetourTime,
                seats = 3, // FIXME what is reasonable here?
                autocommit = false, // FIXME provide gui component
                max_detour_time = maxDetourTime
        )
    }

    private fun getMarkerColor(index: Int) : Int {
        return ContextCompat.getColor(this, getMarkerColorResourceId(index))
    }

    private fun onSelectedItemsChanged(allLiftInfos: List<LiftInfo>, selectedLiftInfos: List<LiftInfo>) {
        mapFragment.removeAllMarkers()
        allLiftInfos.forEachIndexed { index, liftInfo->
            val title = getString(R.string.pickup_point)+" "+liftInfo.partner.first_name    // FIXME should be nickname
            mapFragment.drawStartMarker(
                    liftInfo.lift.pick_up_point,
                    { action -> withProfilePicture(liftInfo) { bitmap -> action(bitmap) } },
                    getMarkerColor(index),
                    title,
                    liftInfo.lift.id)

        }
        selectedLiftInfos.forEach { liftInfo->
            withProfilePicture(liftInfo) {
                val index = allLiftInfos.indexOf(liftInfo)
                val title = getString(R.string.dropoff_point)+" "+liftInfo.partner.first_name   // FIXME should be nickname
                mapFragment.drawDestinationMarker(liftInfo.lift.drop_off_point, getMarkerColor(index), title, liftInfo.lift.id)
            }
        }
        mapFragment.drawDestinationMarker(destination!!, ContextCompat.getColor(this, R.color.driverRouteColor), getString(R.string.label_destination), "self")

        this.selectedLiftInfos = selectedLiftInfos

        api.planRoute(way!!, selectedLiftInfos, ::onRouteFailure, ::onChangedRouteResponse)

    }

    private fun onRouteFailure() {
        startButton.isEnabled = false
        hideKeyboard()
    }

    @OnClick(R.id.start_button)
    fun onStartButtonClick() {
        // change status of all selected Ways to Accepted
        way = way!!.copy(status = Way.Status.STARTED)
        api.changeWayStatus(way!!.id, Way.Status.STARTED)
        selectedLiftInfos?.forEach {
            api.changeLiftStatus(way!!.id, it.lift.id, Lift.Status.ACCEPTED)
        }
        // FIXME Non selected lifts should be deleted now. This should be better done on the server on change of the way status
        // Problem: Lift status and Way status change needs to be done in one request, because otherwise lifts could be deleted before they are accepted
        val navigationPreparer = NavigationPreparer(this, true)
        navigationPreparer.launchNavigationWithRoute(route!!)
    }

    @OnClick(R.id.new_search_button)
    fun onNewSearchButtonClick() {
        isAutocompletionActive = true
        searchFragment.clear()
        searchFrame.visibility = VISIBLE
        searchFragment.focus()
    }


    override fun setFocusIndex(index: Int) {
    }

}