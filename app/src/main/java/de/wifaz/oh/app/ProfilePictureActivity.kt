package de.wifaz.oh.app

import android.content.Intent
import android.graphics.BitmapFactory
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.Space
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.google.android.material.snackbar.Snackbar
import de.wifaz.oh.app.R
import java.io.File
import java.nio.file.Files
import java.nio.file.StandardCopyOption

class ProfilePictureActivity : HitchActivity() {
    companion object {
        val PARAMETER_MODE = "mode"
        val MODE_REGISTRATION = "registration"
        val MODE_SETTINGS = "settings"
    }

    private val REQUEST_CROP: Int = 3

    @BindView(R.id.all)
    lateinit var rootView: View

    @BindView(R.id.form_without_picture)
    lateinit var formWithoutPicture: View

    @BindView(R.id.form_with_picture)
    lateinit var formWithPicture: View

    @BindView(R.id.change_button)
    lateinit var changeButton: Button

    @BindView(R.id.space_before_upload_button)
    lateinit var spaceBeforeUploadButton: Space

    @BindView(R.id.upload_button)
    lateinit var uploadButton: Button

    @BindView(R.id.camera_button)
    lateinit var cameraButton: Button

    @BindView(R.id.gallery_button)
    lateinit var galleryButton: Button

    @BindView(R.id.profile_picture_view)
    lateinit var profilePictureView: ImageView

    lateinit var rawProfilePictureFile: File
    lateinit var newProfilePictureFile: File
    lateinit var currentProfilePictureFile: File

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile_picture)
        ButterKnife.bind(this)
        val mode = intent.getStringExtra(PARAMETER_MODE)
        val title =
            if (MODE_REGISTRATION == mode) getString(R.string.registration) else getString(R.string.profile_picture)
        initActionBar(title, false)
        initImagePaths()
        updatePictureDisplay()
    }

    override fun onDestroy() {
        super.onDestroy()
        if (newProfilePictureFile.exists()) {
            newProfilePictureFile.delete()
        }
    }

    private fun initImagePaths() {
        val dir = getExternalFilesDir("profile_picture")
            ?: throw HitchAppError("Could not get cache directory for profile pictures")
        val path = dir.absolutePath
        rawProfilePictureFile = File(path, "raw.jpg")
        newProfilePictureFile = File(path, "new.jpg")
        currentProfilePictureFile = File(path, "current.jpg")
    }


    private fun updatePictureDisplay() {
        if (newProfilePictureFile.exists()) {
            val bitmap = BitmapFactory.decodeFile(newProfilePictureFile.absolutePath)
            profilePictureView.setImageBitmap(bitmap)
            formWithPicture.visibility = View.VISIBLE
            formWithoutPicture.visibility = View.GONE
            uploadButton.visibility = View.VISIBLE
            spaceBeforeUploadButton.visibility = View.VISIBLE
        } else if (currentProfilePictureFile.exists()) {
            val bitmap = BitmapFactory.decodeFile(currentProfilePictureFile.absolutePath)
            profilePictureView.setImageBitmap(bitmap)
            formWithPicture.visibility = View.VISIBLE
            formWithoutPicture.visibility = View.GONE
            spaceBeforeUploadButton.visibility = View.GONE
            uploadButton.visibility = View.GONE
        } else {
            formWithPicture.visibility = View.GONE
            formWithoutPicture.visibility = View.VISIBLE
        }
    }

    @OnClick(R.id.change_button)
    fun onChangeButtonClick() {
        startCropActivity(CropActivity.SOURCE_NONE)
    }

    private fun startCropActivity(source: Int) {
        val intent = Intent(this, CropActivity::class.java)
        intent.putExtra(CropActivity.RAW_IMAGE_PATH_PARAMETER, rawProfilePictureFile.absolutePath)
        intent.putExtra(CropActivity.CROPPED_IMAGE_PATH_PARAMETER, newProfilePictureFile.absolutePath)
        intent.putExtra(CropActivity.PARAMETER_SOURCE, source)
        startActivityForResult(intent, REQUEST_CROP)
    }

    @OnClick(R.id.upload_button)
    fun onUploadButtonClick() {
        api.account.uploadProfilePicture(newProfilePictureFile) {
            moveNewPictureToCurrent()
            setResult(RESULT_OK, Intent())
            finish()
        }
    }

    @OnClick(R.id.camera_button)
    fun onCameraButtonClick() {
        startCropActivity(CropActivity.SOURCE_CAMERA)
    }

    @OnClick(R.id.gallery_button)
    fun onGaleryButtonClick() {
        startCropActivity(CropActivity.SOURCE_GALERY)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            REQUEST_CROP -> {
                if (resultCode == RESULT_OK) {
                    updatePictureDisplay()
                } else {
                    Snackbar.make(rootView, getString(R.string.take_picture_failed), Snackbar.LENGTH_SHORT).show()
                }
            }
            else -> {
                throw HitchAppError("Unexpected request code in ProfilePictureActivity, requestCode=${requestCode}")
            }
        }
    }

    private fun moveNewPictureToCurrent() {
        if (!newProfilePictureFile.exists()) {
            throw HitchAppError("New profile picture does not exist after upload")
        }
        Files.move(
            newProfilePictureFile.toPath(),
            currentProfilePictureFile.toPath(),
            StandardCopyOption.REPLACE_EXISTING
        )
    }

}