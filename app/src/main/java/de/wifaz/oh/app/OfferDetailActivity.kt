package de.wifaz.oh.app

import android.Manifest
import android.content.Context
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.View
import android.view.inputmethod.InputMethodManager
import android.widget.Button
import android.widget.TextView
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import de.wifaz.oh.app.R
import com.mapbox.api.directions.v5.models.DirectionsRoute
import com.mapbox.mapboxsdk.Mapbox
import com.mapbox.mapboxsdk.maps.MapView
import com.mapbox.mapboxsdk.maps.MapboxMap
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback
import de.wifaz.oh.app.HitchComponent.Companion.PARAMETER_WAY_ID
import de.wifaz.oh.protocol.Lift
import de.wifaz.oh.protocol.LiftInfo
import de.wifaz.oh.protocol.OHPoint
import de.wifaz.oh.protocol.Way
import java.util.Date
import java.util.EnumSet

class OfferDetailActivity : HitchActivity(), OnMapReadyCallback {
    private val LOCATION_PERMISSION_REQUEST_CODE: Int = 1

    @BindView(R.id.overline)
    lateinit var overlineView: TextView

    @BindView(R.id.destination_point_view)
    lateinit var destinationPointView: TextView

    @BindView(R.id.distance_view)
    lateinit var distanceView: TextView

    @BindView(R.id.arrival_time_view)
    lateinit var arrivalTimeView: TextView

    @BindView(R.id.start_button)
    lateinit var startButton: Button

    @BindView(R.id.confirm_choice_button)
    lateinit var confirmChoiceButton: Button

    private lateinit var mapFragment: MapFragment
    private val mapView: MapView
        get() = mapFragment.mapView

    private lateinit var wayId: String
    private lateinit var way: Way

    private lateinit var start: OHPoint
    private lateinit var destination: OHPoint

    private var allLiftInfos: List<LiftInfo> = emptyList()
    private var selectedLiftInfos: List<LiftInfo> = emptyList()
    private var route: DirectionsRoute? = null

    private var numberOfRequests = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_offer_detail)
        initActionBar(getString(R.string.ride_offer))
        Mapbox.getInstance(this.applicationContext, getString(R.string.mapbox_access_token))
        ButterKnife.bind(this)

        val extras = intent.extras
        wayId = extras!!.getString(PARAMETER_WAY_ID)!!
        way = api.ways.getWay(wayId)
        start = way.waypoints[0]
        destination = way.waypoints.last()

        mapFragment = supportFragmentManager.findFragmentById(R.id.map_fragment) as MapFragment
        mapFragment.setOnMapReadyCallback(this)

        initTextViews()

        observe(api.ways.liftInfoCountStream(wayId, EnumSet.of(Lift.Status.REQUESTED))) {
            numberOfRequests = it
            updateButtons()
        }
    }

    private fun updateButtons() {
        confirmChoiceButton.visibility = visibility(numberOfRequests!=0)
        val now = Date().time
        startButton.visibility = visibility(route!=null && numberOfRequests==0)
        startButton.isEnabled = way.start_time < now+30*MINUTE
    }

    private fun visibility(b:Boolean) : Int {
        return if(b) {
            View.VISIBLE
        } else {
            View.GONE
        }
    }

    private fun initPassengerList() {
        val passengerListFragment =
                supportFragmentManager.findFragmentById(R.id.passenger_list) as PassengerListFragment
        passengerListFragment.initialize(way, ::onSelectedItemsChanged)

    }

    private fun initTextViews() {
        val dateTimeSpec = Util.formatDateTimeSpecification(this, Date(way.start_time))
        overlineView.text = getString(R.string.your_ride_at_to_format).format(dateTimeSpec)

        destinationPointView.text = way.waypoints.last().toString()
    }

    override fun onStart() {
        super.onStart()
        LocationMemory.load(this)
    }

    override fun onStop() {
        LocationMemory.save(this)
        super.onStop()
    }

    override fun onMapReady(mapboxMap: MapboxMap) {
        api.fetchRoute(way.waypoints, {})  {route->
            mapFragment.drawRoute(route, MapFragment.DRIVER_ROUTE_SOURCE_ID)
            distanceView.text = Util.formatDistance(this, route.distance()!!)
        }
        initPassengerList()
    }

    fun onChangedRouteResponse(route: DirectionsRoute) {
        this.route = route
        updateButtons()

        mapFragment.drawRoute(route, MapFragment.DRIVER_ROUTE_SOURCE_ID)
        updateRouteInformation(route)
    }


    private fun updateRouteInformation(route: DirectionsRoute) {
        distanceView.text = Util.formatDistance(this, route.distance()!!)
        val arrivalTime = way.start_time + route.duration()!!.toLong() * SECOND
        arrivalTimeView.text = Util.formatTime(this, Date(arrivalTime))
    }

    private fun onSelectedItemsChanged(allLiftInfos: List<LiftInfo>, selectedLiftInfos: List<LiftInfo>) {
        mapFragment.removeAllMarkers()
        allLiftInfos.forEachIndexed { index,liftInfo->
            withProfilePicture(liftInfo) {
                val title = getString(R.string.pickup_point)+" "+liftInfo.partner.first_name    // FIXME should be nickname
                mapFragment.drawStartMarker(
                        liftInfo.lift.pick_up_point,
                        {action->withProfilePicture(liftInfo) {bitmap->action(bitmap)}},
                        ContextCompat.getColor(this, getMarkerColorResourceId(index)),
                        title,
                        liftInfo.partner.id
                )
            }
        }
        selectedLiftInfos.forEach { liftInfo->
            withProfilePicture(liftInfo) {
                val index = allLiftInfos.indexOf(liftInfo)
                val title = getString(R.string.dropoff_point)+" "+liftInfo.partner.first_name   // FIXME should be nickname
                val color = ContextCompat.getColor(this, getMarkerColorResourceId(index))
                mapFragment.drawDestinationMarker(liftInfo.lift.drop_off_point,color,title,liftInfo.lift.id)
            }
        }
        mapFragment.drawDestinationMarker(destination, ContextCompat.getColor(this, R.color.driverRouteColor), getString(R.string.label_destination),"self")

        val routedLiftInfos = selectedLiftInfos + allLiftInfos.filter { it.lift.status!=Lift.Status.REQUESTED }

        this.selectedLiftInfos = selectedLiftInfos
        this.allLiftInfos = allLiftInfos

        api.planRoute(way, routedLiftInfos, ::onRouteFailure, ::onChangedRouteResponse)
    }

    private fun onRouteFailure() {
        route = null
        // FIXME clear route from map
        updateButtons()
        hideKeyboard()
    }

    @OnClick(R.id.cancel_button)
    fun cancelTour() {
        api.deleteWay(wayId) {
            finish()
        }
    }

    @OnClick(R.id.confirm_choice_button)
    fun onConfirmChoiceButtonClick() {
        allLiftInfos.forEach {
            if (it in selectedLiftInfos) {
                api.changeLiftStatus(way.id, it.lift.id, Lift.Status.ACCEPTED)
            } else if (it.lift.status==Lift.Status.REQUESTED) {
                api.changeLiftStatus(way.id, it.lift.id, Lift.Status.REJECTED)
            }
        }
    }

    @OnClick(R.id.start_button)
    fun onStartButtonClick() {
        // change status of all selected Ways to Accepted
        way = way.copy(status = Way.Status.STARTED)
        api.changeWayStatus(way.id, Way.Status.STARTED)
        selectedLiftInfos.forEach {
            api.changeLiftStatus(way.id, it.lift.id, Lift.Status.ACCEPTED)
        }
        // FIXME Non selected lifts should be deleted now. This should be better done on the server on change of the way status
        // Problem: Lift status and Way status change needs to be done in one request, because otherwise lifts could be deleted before they are accepted
        val navigationPreparer = NavigationPreparer(this, true)
        navigationPreparer.launchNavigationWithRoute(route!!)
    }

}