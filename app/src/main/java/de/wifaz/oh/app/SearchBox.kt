package de.wifaz.oh.app

import de.wifaz.oh.app.R
import com.mapbox.geojson.Point
import com.mapbox.mapboxsdk.geometry.LatLng
import com.mapbox.mapboxsdk.maps.MapView
import com.mapbox.mapboxsdk.maps.MapboxMap
import de.wifaz.oh.protocol.OHPoint
import java.util.*


class SearchBox : SearchLocationListener {

    private lateinit var searchLocationFragments: MutableList<SearchLocationFragment>

    lateinit var activity: ChooseWaypointsActivity
    private lateinit var mapView: MapView
    private lateinit var mapboxMap: MapboxMap
    private var focusIndex: Int = 0

    val waypoints: ArrayList<OHPoint>?
        get() {
            val startPoint = getWayPoint(0)
            val destinationPoint = destinationPoint
            return if (startPoint == null || destinationPoint == null) {
                null
            } else ArrayList(Arrays.asList(startPoint, destinationPoint))
        }

    private val destinationPoint: OHPoint?
        get() = searchLocationFragments.last().waypoint

    fun init(activity: ChooseWaypointsActivity) {
        this.activity = activity
        mapboxMap = activity.mapboxMap
        mapView = activity.mapView

        initSearchLocationFragments()
    }

    fun getFragment(resourceId: Int): SearchLocationFragment {
        val fm = activity.supportFragmentManager
        return fm.findFragmentById(resourceId) as SearchLocationFragment
    }

    fun initSearchLocationFragments() {
        searchLocationFragments = ArrayList()
        val start = getFragment(R.id.start_chooser)
        searchLocationFragments.add(start)
        start.init(activity.mapFragment, this, 0, true)
        val destination = getFragment(R.id.destination_chooser)
        searchLocationFragments.add(destination)
        destination.init(activity.mapFragment, this, 1, true)
        start.setLabel(activity.getString(R.string.label_start))
        destination.setLabel(activity.getString(R.string.label_destination))
        val success = start.setToCurrentLocation()
        setFocus(if(success) 1 else 0)
    }

    private fun setFocus(index: Int) {
        setFocusIndex(index)
        searchLocationFragments[index].focus()
    }

    override fun setFocusIndex(index: Int) {
        focusIndex = index
        val start = searchLocationFragments[0]
        val destination = searchLocationFragments[1]
        if (index==0) {
            start.setLabel(activity.getString(R.string.label_start) + " - " +  activity.getString(R.string.search_or_click))
            destination.setLabel(activity.getString(R.string.label_destination))
        } else if (index==1) {
            start.setLabel(activity.getString(R.string.label_start))
            destination.setLabel(activity.getString(R.string.label_destination) + " - " +  activity.getString(R.string.search_or_click))
        }
    }

    private fun getWayPoint(index: Int): OHPoint? {
        return searchLocationFragments[index].waypoint
    }


    override fun updateRoute() {
        activity.updateRoute(waypoints)
    }

    fun setPoint(ohPoint: OHPoint) {
        val searchLocationFragment = searchLocationFragments[focusIndex]
        searchLocationFragment.waypoint = ohPoint
        searchLocationFragment.startReverseSearch(ohPoint)
    }

}
