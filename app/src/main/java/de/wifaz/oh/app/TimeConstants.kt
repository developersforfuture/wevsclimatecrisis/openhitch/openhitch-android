package de.wifaz.oh.app

val SECOND = 1000
val MINUTE = 60 * SECOND
val HOUR = 60 * MINUTE
val DAY = 24 * HOUR