package de.wifaz.oh.app

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.View
import android.widget.Button
import androidx.core.content.FileProvider
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.avito.android.krop.KropView
import com.google.android.material.snackbar.Snackbar
import de.wifaz.oh.app.R
import java.io.File
import java.io.FileOutputStream
import java.io.OutputStream

class CropActivity : HitchActivity() {
    companion object {
        val PARAMETER_SOURCE = "source"
        val SOURCE_NONE = 0
        val SOURCE_CAMERA = 1
        val SOURCE_GALERY = 2
        val RAW_IMAGE_PATH_PARAMETER = "rawImagePath"
        val CROPPED_IMAGE_PATH_PARAMETER = "croppedImagePath"
    }

    private val REQUEST_CAPTURE_IMAGE: Int = 1
    private val REQUEST_SELECT_IMAGE: Int = 2

    @BindView(R.id.layout_initial)
    lateinit var initialLayout: View

    @BindView(R.id.layout_normal)
    lateinit var normalLayout: View

    @BindView(R.id.submit_button)
    lateinit var submitButton: Button

    @BindView(R.id.take_picture_button)
    lateinit var takePictureButton: Button

    @BindView(R.id.select_picture_button)
    lateinit var selectPictureButton: Button

    @BindView(R.id.all)
    lateinit var rootView: View

    @BindView(R.id.krop_view)
    lateinit var kropView: KropView

    lateinit var rawImageFile: File
    lateinit var croppedImageFile: File

    var isViewInitialized = false

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        rawImageFile = File(intent.getStringExtra(RAW_IMAGE_PATH_PARAMETER)!!)
        croppedImageFile = File(intent.getStringExtra(CROPPED_IMAGE_PATH_PARAMETER)!!)

        val source = intent.getIntExtra(PARAMETER_SOURCE, SOURCE_NONE)
        when (source) {
            SOURCE_CAMERA -> openCameraIntent()
            SOURCE_GALERY -> selectPicture()
            SOURCE_NONE -> initView()
        }
    }

    private fun initView() {
        if (!isViewInitialized) {
            setContentView(R.layout.activiy_crop)
            ButterKnife.bind(this)
            isViewInitialized = true
        }
        setBitmap()
    }

    private fun setBitmap() {
        if (rawImageFile.exists()) {
            val myBitmap = BitmapFactory.decodeFile(rawImageFile.absolutePath)
            kropView.setBitmap(myBitmap)
            initialLayout.visibility = View.GONE
            normalLayout.visibility = View.VISIBLE

        } else {
            initialLayout.visibility = View.VISIBLE
            normalLayout.visibility = View.GONE
        }
    }

    @OnClick(R.id.submit_button)
    fun submitButtonClick() {
        kropView.getCroppedBitmap()?.let {
            val resizedImage = if (it.width <= 360) it else Bitmap.createScaledBitmap(it, 360, 360, false)
            storeCroppedImage(resizedImage)
        }
        setResult(Activity.RESULT_OK)
        finish()
    }

    private fun storeCroppedImage(image: Bitmap) {
        val fos = FileOutputStream(croppedImageFile)
        image.compress(Bitmap.CompressFormat.JPEG, 90, fos)
        fos.close()
    }

    @OnClick(R.id.take_picture_button)
    fun takePictureButtonPress() {
        openCameraIntent()
    }

    @OnClick(R.id.take_picture_button_initial)
    fun initialTakePictureButtonPress() {
        openCameraIntent()
    }


    // cf. https://www.oodlestechnologies.com/blogs/Capture-Image-From-Your-Camera-Using-File-Provider-In-Android/
    private fun openCameraIntent() {
        val pictureIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
        if (pictureIntent.resolveActivity(packageManager) != null) {
            val photoURI: Uri = FileProvider.getUriForFile(this, "de.wifaz.oh.provider", rawImageFile)
            pictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
            startActivityForResult(pictureIntent, REQUEST_CAPTURE_IMAGE)
        }
    }

    @OnClick(R.id.select_picture_button)
    fun selectPictureButtonPress() {
        selectPicture()
    }

    @OnClick(R.id.select_picture_button_initial)
    fun initialSelectPictureButtonPress() {
        selectPicture()
    }


    private fun selectPicture() {
        val intent = Intent(Intent.ACTION_GET_CONTENT)
        intent.type = "image/*"
        startActivityForResult(intent, REQUEST_SELECT_IMAGE)

    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        when (requestCode) {
            REQUEST_CAPTURE_IMAGE -> {
                if (resultCode == RESULT_OK) {
                    // nothing to do
                } else {
                    Snackbar.make(rootView, getString(R.string.take_picture_failed), Snackbar.LENGTH_SHORT).show()
                }
                initView()
            }
            REQUEST_SELECT_IMAGE -> {
                var selectedImageUri: Uri? = null
                if (resultCode == RESULT_OK) {
                    selectedImageUri = data?.data
                }
                if (selectedImageUri == null) {
                    Snackbar.make(rootView, getString(R.string.no_picture_selected), Snackbar.LENGTH_SHORT).show()
                    return
                }

                saveFile(selectedImageUri, rawImageFile)
                initView()
            }
            else -> {
                throw HitchAppError("Unexpected request code in CropActivity, requestCode=${requestCode}")
            }
        }
    }

    private fun saveFile(sourceUri: Uri, destination: File) {
        val src = contentResolver.openInputStream(sourceUri)!!
        val buffer = ByteArray(src.available())
        src.read(buffer)

        val outStream: OutputStream = FileOutputStream(destination)
        outStream.write(buffer)
    }

}

