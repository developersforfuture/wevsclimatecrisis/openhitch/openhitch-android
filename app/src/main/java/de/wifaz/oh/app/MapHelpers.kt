package de.wifaz.oh.app

private val markerColorResourceIds = arrayOf(
    R.color.passengerMarkerColor0,
    R.color.passengerMarkerColor1,
    R.color.passengerMarkerColor2,
    R.color.passengerMarkerColor3,
    R.color.passengerMarkerColor4,
    R.color.passengerMarkerColor5,
)

fun getMarkerColorResourceId(index:Int) : Int{
    return markerColorResourceIds[index % markerColorResourceIds.size]
}