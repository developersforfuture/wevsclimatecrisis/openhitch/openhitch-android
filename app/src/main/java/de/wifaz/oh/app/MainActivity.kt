package de.wifaz.oh.app

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import de.wifaz.oh.app.BuildConfig
import de.wifaz.oh.app.R
import de.wifaz.oh.app.HitchComponent.Companion.PARAMETER_WAY_ID
import de.wifaz.oh.protocol.Role
import de.wifaz.oh.protocol.Way


class MainActivity : HitchActivity() {
    @BindView(R.id.list_button)
    lateinit var listButton: Button

    @BindView(R.id.infobox)
    lateinit var infobox: TextView

    @BindView(R.id.persona_button)
    lateinit var personaButton: Button

    var uniquePlannedWay : Way? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        ButterKnife.bind(this)
        initActionBar("OpenHitch")

        infobox.text = getText(R.string.welcome_text)
        if (BuildConfig.DEBUG) {
            personaButton.visibility = View.VISIBLE
        }
    }

    override fun onStart() {
        super.onStart()
        observe(api.ways.wayListStream({ it.status == Way.Status.PUBLISHED })) {
            updateListButton(it)
        }
    }


    private fun updateListButton(ways: List<Way>) {
        val size = ways.size
        if (size==1) {
            uniquePlannedWay = ways[0]
        } else {
            uniquePlannedWay = null
        }
        if (size == 0) {
            listButton.visibility = View.GONE
        } else {
            listButton.visibility = View.VISIBLE
            listButton.text = resources.getQuantityString(R.plurals.ride_list_title, size, size)
        }
    }

    @OnClick(R.id.offer_spontaneous_button)
    fun onOfferSpontaneousButtonClick() {
        val intent = Intent(this, SpontaneousRideActivity::class.java)
        startActivity(intent)
    }


    @OnClick(R.id.offer_button)
    fun onOfferButtonClick() {
        val intent = Intent(this, ChooseWaypointsActivity::class.java)
        val extras = Bundle()
        extras.putSerializable(ChooseWaypointsActivity.PARAMETER_ROLE, Role.DRIVER)
        intent.putExtras(extras)
        startActivity(intent)
    }

    @OnClick(R.id.search_button)
    fun onSearchButtonClick() {
        val intent = Intent(this, ChooseWaypointsActivity::class.java)
        val extras = Bundle()
        extras.putSerializable(ChooseWaypointsActivity.PARAMETER_ROLE, Role.PASSENGER)
        intent.putExtras(extras)
        startActivity(intent)
    }

    @OnClick(R.id.list_button)
    fun onListButtonClick() {
        uniquePlannedWay?.let() {
            if(it.role==Role.DRIVER) {
                val intent = Intent(this, OfferDetailActivity::class.java)
                intent.putExtra(PARAMETER_WAY_ID,it.id)
                startActivity(intent)
            } else {
                val intent = Intent(this, SearchDetailActivity::class.java)
                intent.putExtra(PARAMETER_WAY_ID,it.id)
                startActivity(intent)
            }
        } ?: run {
            val intent = Intent(this, MyWaysActivity::class.java)
            startActivity(intent)
        }
    }

    @OnClick(R.id.persona_button)
    fun onPersonaButtonClick() {
        showPersonaChooserDialog(this)
    }

}
