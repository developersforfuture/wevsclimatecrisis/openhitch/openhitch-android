package de.wifaz.oh.app

import android.location.Location
import com.mapbox.geojson.Point
import com.mapbox.mapboxsdk.geometry.LatLng
import de.wifaz.oh.protocol.OHPoint

fun Point.toOHPoint(): OHPoint {
    return OHPoint.fromLngLat(longitude(), latitude())
}

fun OHPoint.toLatLng(): LatLng {
    return LatLng(latitude, longitude)
}

fun LatLng.toLatLngString(): String {
    return latitude.toString() + "," + longitude.toString()
}

fun LatLng.toPoint(): Point {
    return Point.fromLngLat(longitude, latitude)
}

fun LatLng.toOHPoint(): OHPoint {
    return OHPoint(longitude = longitude, latitude = latitude)
}


fun Location.toPoint(): Point {
    return Point.fromLngLat(longitude, latitude)
}

fun Location.toOHPoint(): OHPoint {
    return OHPoint.fromLngLat(longitude, latitude)
}
