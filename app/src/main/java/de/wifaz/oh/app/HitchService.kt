package de.wifaz.oh.app

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.Service
import android.content.Intent
import android.graphics.BitmapFactory
import android.os.Binder
import android.os.Build
import android.os.IBinder
import androidx.core.app.NotificationCompat
import de.wifaz.oh.app.R
import de.wifaz.oh.api.*
import timber.log.Timber

// Service is autostarted on boot as described here: https://stackoverflow.com/questions/7690350/android-start-service-on-boot
// (currently not: removed Autostart from AndroidManifest.xml)

class HitchService : Service() {

    // cf https://developer.android.com/guide/components/bound-services.html#Binder
    // Binder given to clients
    private val binder = LocalBinder()

    /**
     * Class used for the client Binder.  Because we know this service always
     * runs in the same process as its clients, we don't need to deal with IPC.
     */
    inner class LocalBinder : Binder() {
        // Return this instance of LocalService so clients can call public methods
        val service: HitchService
            get() = this@HitchService
    }

    override fun onBind(intent: Intent?): IBinder? {
        return binder
    }

    override fun onCreate() {
        createNotificationChannel()
    }

    override fun onStartCommand(
        intent: Intent?,
        flags: Int,
        startId: Int
    ): Int {
        Timber.i("entered onStartCommand")
        val notification = createNotification()
        startForeground(HITCH_SERVICE_NOTIFICATION_ID, notification)
        Timber.i("started")

        return Service.START_STICKY
    }


    override fun onDestroy() {
        Timber.i("HitchService stopped")
    }

    private fun createNotification(): Notification {
        val icon = BitmapFactory.decodeResource(
            resources,
            R.drawable.ic_thumb_up_black_24dp
        )

        return NotificationCompat.Builder(this, NOTIFICATION_CHANNEL_ID)
            // FIXME use resource strings
            .setContentTitle(getString(R.string.hitchservice_started))
            .setContentText(getString(R.string.hitchservice_description))

            // FIXME icon was null on boot
            // .setSmallIcon(R.drawable.ic_thumb_up_black_24dp)
            // .setLargeIcon(
            //        Bitmap.createScaledBitmap(icon, 128, 128, false))
            .setOngoing(true)
            .build()
    }

    private fun createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            return
        }

        val notificationManager = getSystemService<NotificationManager>(NotificationManager::class.java)
            ?: throw HitchServiceInternalException("Could not create NotificationManager")

        val generalChannel =
            NotificationChannel(NOTIFICATION_CHANNEL_ID, "OpenHitch", NotificationManager.IMPORTANCE_DEFAULT)
        generalChannel.description = "OpenHitch Meldungen"  // FIXME resource string
        // Register the channel with the system; you can't change the importance
        // or other notification behaviors after this
        notificationManager.createNotificationChannel(generalChannel)
    }

    companion object {
        private val HITCH_SERVICE_NOTIFICATION_ID = 1
        val NOTIFICATION_CHANNEL_ID = "OpenHitch"
    }
}

// FIXME should be non-Runtime-Exceptions and properly caught
class HitchServiceInternalException(message: String) : java.lang.RuntimeException(message)

class HitchProtocolException(message: String) : java.lang.RuntimeException(message)
