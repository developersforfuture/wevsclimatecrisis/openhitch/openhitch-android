package de.wifaz.oh.app

import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.util.Base64
import android.widget.Button
import android.widget.LinearLayout
import android.widget.TextView
import com.fasterxml.jackson.annotation.JsonInclude
import com.fasterxml.jackson.databind.ObjectMapper
import de.wifaz.oh.app.BuildConfig
import de.wifaz.oh.app.R
import de.wifaz.oh.api.Api
import de.wifaz.oh.protocol.HitchDebugInterface
import de.wifaz.oh.protocol.HitchInterface
import de.wifaz.oh.protocol.RegistrationState
import de.wifaz.oh.protocol.User
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.jackson.JacksonConverterFactory

fun showPersonaChooserDialog(activity: HitchActivity) {
    if (!BuildConfig.DEBUG) {
        return
    }

    val dialog = PersonaChooserDialog(activity)
    dialog.show()
}

class PersonaChooserDialog(private val activity: HitchActivity) : Dialog(activity) {
    val api = activity.api
    init {
        setContentView(R.layout.dialog_persona_chooser)
        val layout = findViewById<LinearLayout>(R.id.layout)
        val currentUserView = findViewById<TextView>(R.id.current_user_view)
        currentUserView.text = api.account.user?.first_name ?: "--Unregistered--"

        val paramsButton =
            LinearLayout.LayoutParams(LinearLayout.LayoutParams.WRAP_CONTENT, LinearLayout.LayoutParams.WRAP_CONTENT)

        val debugRetrofitClient = createDebugRetrofitClient()
        val call = debugRetrofitClient.getPersonas()
        api.callHandler.enqueueCall(call,"Fetching personas") { personas->
            for (persona in personas) {
                val button = Button(activity)
                button.text = persona.first_name
                button.layoutParams = paramsButton
                button.setOnClickListener {
                    val token = Base64.encodeToString(persona.id.encodeToByteArray(), Base64.DEFAULT.or(Base64.NO_WRAP))
                    val rs = RegistrationState(persona.id, true, false, true)
                    api.account.setUser(persona, rs, token)
                    api.sync()
                    dismiss()
                    activity.startActivity(Intent(activity, LauncherActivity::class.java))
                    activity.finish()
                }
                layout.addView(button)
            }
            val button = Button(activity)
            button.text = "--Unregistered--"
            button.layoutParams = paramsButton
            button.setOnClickListener {
                val intent = Intent(activity, LauncherActivity::class.java)
                api.account.setUser(null, null, null)
                api.sync()
                dismiss()
                activity.startActivity(intent)
                activity.finish()
            }
            layout.addView(button)
        }
    }

    private fun createDebugRetrofitClient(): HitchDebugInterface {
        val hitchApiUrl: String = activity.getString(R.string.hitch_debug_url)

        val objectMapper = ObjectMapper()
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL)
        val httpClient = OkHttpClient.Builder()
            .build()
        val retrofit = Retrofit.Builder()
            .baseUrl(hitchApiUrl)
            .addConverterFactory(JacksonConverterFactory.create(objectMapper))
            .client(httpClient)
            .build()
        return retrofit.create(HitchDebugInterface::class.java)
    }

}