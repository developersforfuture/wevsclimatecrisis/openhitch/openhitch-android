package de.wifaz.oh.app

import android.os.Bundle

import de.wifaz.oh.app.R

class MyWaysActivity : HitchActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_my_ways)
        initActionBar(title = getString(R.string.my_ways))
    }

}
