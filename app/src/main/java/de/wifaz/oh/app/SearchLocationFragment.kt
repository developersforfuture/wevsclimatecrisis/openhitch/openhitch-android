package de.wifaz.oh.app

import android.content.Context
import android.os.Bundle
import android.view.KeyEvent
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.AdapterView
import android.widget.BaseAdapter
import android.widget.Filter
import android.widget.Filterable
import android.widget.TextView
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import butterknife.OnFocusChange
import butterknife.OnTextChanged
import butterknife.OnTouch
import com.google.android.material.textfield.TextInputLayout
import de.wifaz.oh.app.R
import com.mapbox.geojson.Point
import com.mapbox.mapboxsdk.maps.MapView
import com.mapbox.mapboxsdk.maps.MapboxMap
import de.wifaz.oh.api.CURRENT_POSITION_MARKER
import de.wifaz.oh.protocol.OHPoint
import java.util.ArrayList

interface SearchLocationListener {
    fun updateRoute()
    fun setFocusIndex(index: Int)
}

class SearchLocationFragment : HitchFragment(), AdapterView.OnItemClickListener, TextView.OnEditorActionListener {
    private  var showCurrentLocation: Boolean = false
    private lateinit var listener: SearchLocationListener
    private var index: Int = 0
    lateinit var mapFragment:MapFragment

    @BindView(R.id.text_input_layout)
    lateinit var textInputLayout: TextInputLayout
    @BindView(R.id.edit_text)
    lateinit var editText: AutoCompleteTextViewZeroThreshold

    private var searchResults: MutableList<OHPoint> = ArrayList()
    private var filteredOptions: List<OHPoint> = ArrayList()

    private lateinit var adapter: LocationSearchAdapter

    var waypoint: OHPoint? = null
        set(ohPoint: OHPoint?) {
            field = ohPoint
            listener.updateRoute()
            if (true==ohPoint?.description?.startsWith(CURRENT_POSITION_MARKER)) {
                startReverseSearch(ohPoint)
            }
        }

    private var isAutocompletionActive: Boolean = false
    private var isInitialized: Boolean = false
    private var isSearchRunning = false
    private var pendingQuery: String? = null

    private val mapView:MapView
        get() = mapFragment.mapView
    private val mapboxMap:MapboxMap
        get() = mapFragment.mapboxMap

    fun init(mapFragment: MapFragment, listener: SearchLocationListener,  index: Int=0, showCurrentLocation: Boolean=false) {
        this.listener = listener
        this.index = index
        this.mapFragment = mapFragment
        this.showCurrentLocation = showCurrentLocation
        editText.threshold = 0
        editText.setAdapter(adapter)
        editText.onItemClickListener = this
        editText.setOnEditorActionListener(this)
        isInitialized = true
    }

    private val currentLocation: OHPoint?
        get() {
            val lastKnownLocation = mapFragment.lastKnownLocation
                ?: return null
            val result = lastKnownLocation.toOHPoint()
            result.description = CURRENT_POSITION_MARKER + requireActivity().getString(R.string.current_location)
            return result
        }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_search, container, false)
        ButterKnife.bind(this, view)
        adapter = LocationSearchAdapter(requireActivity(), searchResults)
        return view
    }

    fun focus() {
        isAutocompletionActive = true
        editText.requestFocus()
        editText.showDropDown()
    }

    @OnClick(R.id.edit_text)
    fun onEditTextClick() {
        editText.requestFocus()
    }

    private fun startSearch(query:String) { //DUP
        mapFragment.showLoading()

        synchronized(isSearchRunning) {
            if (isSearchRunning) {
                pendingQuery = query
                return
            } else {
                isSearchRunning = true
            }
        }

        val point = mapboxMap.cameraPosition.target.toOHPoint()

        api.geocodeSearch(query, point, ::onGeosearchError) { ohPoints ->
            searchFinished()
            if (isAutocompletionActive) {
                searchResults.clear()
                searchResults.addAll(ohPoints)
                mapFragment.hideLoading()
                adapter.filter.filter(editText.text)
                editText.showDropDown()
            }
        }
    }

    fun onGeosearchError() {
        searchFinished()
        hideKeyboard()
    }

    private fun hideKeyboard() {
        val imm = requireActivity().getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(mapView.windowToken, 0)
    }


    private fun searchFinished() {
        synchronized(isSearchRunning) {
            isSearchRunning = false
            pendingQuery?.let {
                pendingQuery = null
                startSearch(it)
            }
        }
    }

    fun startReverseSearch(ohPoint: OHPoint, callback:()->Unit = {}) {
        mapFragment.showLoading()
        api.geocodeReverseSearch(ohPoint,{}) { ohPoints ->
            isAutocompletionActive = false
            val description = ohPoints.getOrNull(0)?.description
            waypoint!!.description = description
            editText.setText(description)
            editText.dismissDropDown()
            callback()
        }
    }

    @OnFocusChange(R.id.edit_text)
    fun onFocusChanged(focused: Boolean) {
        if (!isInitialized) {
            return
        }
        if (focused) {
            val text = editText.text.toString()
            if (Util.isEmpty(text) || text.startsWith(CURRENT_POSITION_MARKER)) {
                searchResults.clear()
            }
            adapter.filter.filter(null)
            listener.setFocusIndex(index)
            editText.showDropDown()
        }
    }

    override fun onEditorAction(textView: TextView, i: Int, keyEvent: KeyEvent): Boolean {
        return true
    }

    override fun onItemClick(adapterView: AdapterView<*>, view: View?, i: Int, l: Long) {
        editText.dismissDropDown()
        val ohPoint = adapterView.adapter.getItem(i) as OHPoint
        waypoint = ohPoint
        if (true!=ohPoint.description?.startsWith(CURRENT_POSITION_MARKER)) {
            LocationMemory.add(ohPoint)
        }
    }

    fun setToCurrentLocation() : Boolean {
        val ohPoint = currentLocation
        if (ohPoint != null) {
            editText.setText(ohPoint.description)
            waypoint = currentLocation
        }
        return ohPoint!=null
    }

    fun setLabel(text: String) {
        textInputLayout.setHint(text)
        editText.setHint(text)
    }

    @OnTextChanged(R.id.edit_text)
    fun searchTextChanged() {
        if (isAutocompletionActive && !editText.isPerformingCompletion) {
            if (editText.text.toString().length>=3) {
                startSearch(editText.text.toString())
            } else {
                searchResults.clear()
                adapter.filter.filter(editText.text)
                editText.showDropDown()
            }
        }
    }

    @OnTouch(R.id.edit_text)
    fun onTouch() {
        focus()
    }

    fun clear() {
        editText.text.clear()
    }

    inner class LocationSearchAdapter(val context: Context, val options: MutableList<OHPoint>) : BaseAdapter(), Filterable {
        override fun getCount(): Int {
            return filteredOptions.size
        }

        override fun getItem(i: Int): Any {
            return filteredOptions[i]
        }

        override fun getItemId(i: Int): Long {
            return i.toLong()
        }

        override fun getView(i: Int, view: View?, viewGroup: ViewGroup): View {
            var inflater = context.getSystemService(Context.LAYOUT_INFLATER_SERVICE) as LayoutInflater
            val textView = inflater.inflate(R.layout.autocomplete_item, null) as TextView
            textView.text = filteredOptions[i].description
            return textView
        }

        override fun getFilter(): Filter {
            return LocationSearchFilter()
        }
    }

    inner class LocationSearchFilter : Filter() {

        override fun performFiltering(constraint: CharSequence?): Filter.FilterResults {
            val searchTerms = doSplit(constraint)
            val values = ArrayList<OHPoint>()

            val favorites = ArrayList(LocationMemory.getAll())
            if (showCurrentLocation) {
                val currentLocation = currentLocation
                if (currentLocation != null) {
                    favorites.add(currentLocation)
                }
            }

            for (ohPoint in favorites) {
                if (doesMatch(ohPoint.description, searchTerms)) {
                    values.add(ohPoint)
                }
                if (values.size > 10) {
                    break
                }
            }

            for (ohPoint in searchResults) {
                if (values.size > 10) {
                    break
                }
                if (ohPoint !in values) {
                    values.add(ohPoint)
                }
            }


            val results = Filter.FilterResults()
            results.values = values
            results.count = values.size
            return results
        }

        /**
         * checks if for every search term, description has a part, that starts with that search term
         */
        private fun doesMatch(description: String?, searchTerms: List<String>): Boolean {
            val targetTerms = doSplit(description)
            for (s in searchTerms) {
                if (targetTerms.find { it.startsWith(s) } == null) {
                    return false
                }
            }
            return true
        }

        private fun doSplit(s: CharSequence?): List<String> {
            return (s ?: "")
                    .toString()
                    .toLowerCase()
                    .split("\\W".toRegex())
                    .filter { it.length > 0 }
        }

        override fun publishResults(charSequence: CharSequence?, filterResults: Filter.FilterResults) {
            @Suppress("UNCHECKED_CAST")
            filteredOptions = filterResults.values as List<OHPoint>
            adapter.notifyDataSetChanged()
        }
    }

}