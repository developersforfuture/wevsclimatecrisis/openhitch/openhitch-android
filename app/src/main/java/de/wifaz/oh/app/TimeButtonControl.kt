package de.wifaz.oh.app

import android.app.TimePickerDialog
import android.content.Context
import android.util.AttributeSet
import android.view.View
import android.widget.Button
import android.widget.TimePicker


class TimeButtonControl(private val context: Context, private val button: Button) : View.OnClickListener, TimePickerDialog.OnTimeSetListener {

    var listener: (() -> Unit)? = null

    /**
     * Shown/choosen time in milliseconds since midnight
     */
    var timeOfDay: Int = 0
        set(value) {
            field = value
            button.text = Util.formatTime(context, value)
            listener?.invoke()
        }

    init {
        button.setOnClickListener(this)
    }

    override fun onClick(view: View) {
        val hour = timeOfDay / HOUR
        val minute = (timeOfDay % HOUR) / MINUTE

        val is24HourFormat = android.text.format.DateFormat.is24HourFormat(context)
        val timePickerDialog = TimePickerDialog(context, this, hour, minute, is24HourFormat)
        timePickerDialog.show()
    }

    override fun onTimeSet(p: TimePicker?, hourOfDay: Int, minute: Int) {
        timeOfDay = hourOfDay * HOUR + minute * MINUTE
    }

}
