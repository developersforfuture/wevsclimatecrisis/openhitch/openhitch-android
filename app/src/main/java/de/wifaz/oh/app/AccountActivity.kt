package de.wifaz.oh.app

import android.content.Intent
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import de.wifaz.oh.app.R
import de.wifaz.oh.protocol.User


class AccountActivity : HitchActivity() {
    @BindView(R.id.submit_button)
    lateinit var submitButton: Button

    @BindView(R.id.edit_first_name)
    lateinit var editFirstName: com.google.android.material.textfield.TextInputEditText

    @BindView(R.id.edit_last_name)
    lateinit var editLastName: com.google.android.material.textfield.TextInputEditText

    @BindView(R.id.edit_phone_number)
    lateinit var editPhoneNumber: com.google.android.material.textfield.TextInputEditText

    var currentUser: User? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_account)
        ButterKnife.bind(this)
        initActionBar(getString(R.string.registration), false)

        currentUser = api.account.user
        setTexts(currentUser)
        fillUserData(currentUser)
    }

    private val FIRST_NAME = "firstName"
    private val LAST_NAME = "lastName"
    private val PHONE = "phone"

    override fun onSaveInstanceState(outState: Bundle) {
        outState.putString(FIRST_NAME, editFirstName.text.toString())
        outState.putString(LAST_NAME, editLastName.text.toString())
        outState.putString(PHONE, editPhoneNumber.text.toString())
        super.onSaveInstanceState(outState)
    }

    override fun onRestoreInstanceState(savedInstanceState: Bundle) {
        super.onRestoreInstanceState(savedInstanceState)
        editFirstName.setText(savedInstanceState.getString(FIRST_NAME))
        editLastName.setText(savedInstanceState.getString(LAST_NAME))
        editPhoneNumber.setText(savedInstanceState.getString(PHONE))
    }

    private fun setTexts(user: User?) {
        if (user == null) {
            setActionBarTitle(getString(R.string.registration))
            submitButton.text = getString(R.string.register)
        } else {
            setActionBarTitle(getString(R.string.user_data))
            submitButton.text = getString(R.string.apply)
        }
    }

    private fun EditText.isEmpty(): Boolean {
        return this.text.toString().isEmpty()
    }

    private fun fillUserData(user: User?) {
        // This ist indirectly called after each onStart(). Therefore
        // fill in user data fields only, if they are all empty. Otherwise we would destroy preexisting edits
        // when returning from the CropActivity
        val isEmpty = editFirstName.isEmpty() && editLastName.isEmpty() && editPhoneNumber.isEmpty()
        if (isEmpty) {
            editFirstName.setText(user?.first_name ?: "")
            editLastName.setText(user?.last_name ?: "")
            editPhoneNumber.setText(user?.phone ?: "+49 ")
        }
    }

    fun getNonEmptyValue(editText: EditText): String {
        val value = editText.text.toString()
        if (value == "") {
            editText.error = getString(R.string.field_mandatory)
            throw EmptyFieldException()
        }
        return value
    }

    @OnClick(R.id.submit_button)
    fun onSubmitButtonClick() {
        val user = currentUser
        try {
            val firstName = getNonEmptyValue(editFirstName)
            val lastName = getNonEmptyValue(editLastName)
            val phone = getNonEmptyValue(editPhoneNumber)
            val id = currentUser?.id ?: Util.randomId()
            val newUser = User(
                id = id,
                first_name = firstName,
                last_name = lastName,
                nick_name = "$firstName $lastName",
                phone = phone
            )

            if (user == null) {
                api.account.register(newUser) {
                    setResult(RESULT_OK, Intent())
                    finish()
                }
            } else {
                api.account.updateUser(newUser) {
                    finish()
                }
            }
        } catch (e: EmptyFieldException) {
            // Do nothing, let registration form remain
        }
    }

    class EmptyFieldException : Exception()

}