package de.wifaz.oh.app

import android.Manifest
import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.graphics.Bitmap
import android.graphics.Canvas
import android.graphics.Color
import android.graphics.Paint
import android.graphics.Path
import android.graphics.PorterDuff
import android.graphics.PorterDuffXfermode
import android.location.Location
import android.os.Build
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ProgressBar
import android.widget.Toast
import androidx.annotation.CallSuper
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import androidx.core.graphics.ColorUtils
import butterknife.BindView
import butterknife.ButterKnife
import butterknife.OnClick
import com.google.android.material.floatingactionbutton.FloatingActionButton
import com.google.android.material.snackbar.Snackbar
import com.graphhopper.navigation.example.GHAttributionDialogManager
import de.wifaz.oh.app.R
import com.mapbox.android.core.location.LocationEngineListener
import com.mapbox.android.core.permissions.PermissionsManager
import com.mapbox.api.directions.v5.models.DirectionsRoute
import com.mapbox.core.constants.Constants
import com.mapbox.geojson.Feature
import com.mapbox.geojson.FeatureCollection
import com.mapbox.geojson.LineString
import com.mapbox.mapboxsdk.camera.CameraUpdateFactory
import com.mapbox.mapboxsdk.exceptions.InvalidLatLngBoundsException
import com.mapbox.mapboxsdk.geometry.LatLng
import com.mapbox.mapboxsdk.geometry.LatLngBounds
import com.mapbox.mapboxsdk.maps.MapView
import com.mapbox.mapboxsdk.maps.MapboxMap
import com.mapbox.mapboxsdk.maps.OnMapReadyCallback
import com.mapbox.mapboxsdk.plugins.locationlayer.LocationLayerPlugin
import com.mapbox.mapboxsdk.plugins.locationlayer.modes.RenderMode
import com.mapbox.mapboxsdk.style.expressions.Expression
import com.mapbox.mapboxsdk.style.layers.LineLayer
import com.mapbox.mapboxsdk.style.layers.Property
import com.mapbox.mapboxsdk.style.layers.Property.ICON_ANCHOR_BOTTOM
import com.mapbox.mapboxsdk.style.layers.PropertyFactory
import com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconAllowOverlap
import com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconAnchor
import com.mapbox.mapboxsdk.style.layers.PropertyFactory.iconImage
import com.mapbox.mapboxsdk.style.layers.PropertyFactory.visibility
import com.mapbox.mapboxsdk.style.layers.SymbolLayer
import com.mapbox.mapboxsdk.style.sources.GeoJsonSource
import com.mapbox.mapboxsdk.style.sources.Source
import de.wifaz.oh.api.toPoint
import de.wifaz.oh.app.ChooseWaypointsActivity.Companion.CAMERA_ANIMATION_DURATION
import de.wifaz.oh.protocol.OHPoint
import java.util.ArrayList
import kotlin.math.pow
import kotlin.math.sqrt

open class MapFragment : HitchFragment(), OnMapReadyCallback {
    private val padding = intArrayOf(50, 50, 50, 50)

    @BindView(R.id.mapView)
    lateinit var mapView: MapView

    @BindView(R.id.loading)
    lateinit var loading: ProgressBar

    @BindView(R.id.current_location_fab)
    lateinit var currentLocationFab: FloatingActionButton

    private var locationLayer: LocationLayerPlugin? = null
    lateinit var mapboxMap: MapboxMap
    lateinit var allMarkerIds: MutableList<String>
    //private lateinit var permissionsManager: PermissionsManager

    private var externalOnMapReadyCallback: OnMapReadyCallback? = null

    private var markerScale = 1F

    val lastKnownLocation: Location?
        @SuppressLint("MissingPermission")
        get() = locationLayer?.lastKnownLocation

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        val view = inflater.inflate(R.layout.fragment_map, container, false)
        ButterKnife.bind(this, view)

        return view
    }

    @CallSuper
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)
        val metrics = resources.displayMetrics
        markerScale = metrics.density/2F

        initMap(savedInstanceState)
    }

    private fun initMap(savedInstanceState: Bundle?) {
        mapView.setStyleUrl(getString(R.string.map_view_styleUrl)) // FIXME
        mapView.onCreate(savedInstanceState)
        mapView.getMapAsync(this)
    }

    private fun initDottedLineSourceAndLayer() {
        val dashedLineDirectionsFeatureCollection = FeatureCollection.fromFeatures(arrayOf())
        mapboxMap.addSource(GeoJsonSource("SOURCE_ID", dashedLineDirectionsFeatureCollection))
        mapboxMap.addLayerBelow(
                LineLayer(
                        "DIRECTIONS_LAYER_ID", "SOURCE_ID"
                ).withProperties(
                        PropertyFactory.lineWidth(4.5f),
                        PropertyFactory.lineColor(Color.BLACK),
                        PropertyFactory.lineTranslate(arrayOf(0f, 4f)),
                        PropertyFactory.lineDasharray(arrayOf(1.2f, 1.2f))
                ), "road-label-small"
        )
    }

    /**
     * Add the route layer to the map either using the custom style values or the default.
     */
    private fun addRouteLayer(layerId: String, sourceId: String, color: Int) {
        val scale = 1f
        val featureCollection = FeatureCollection.fromFeatures(arrayOf())
        mapboxMap.addSource(GeoJsonSource(sourceId, featureCollection))
        val routeLayer = LineLayer(layerId, sourceId).withProperties(
                PropertyFactory.lineCap(Property.LINE_CAP_ROUND),
                PropertyFactory.lineJoin(Property.LINE_JOIN_ROUND),
                PropertyFactory.lineWidth(
                        Expression.interpolate(
                                Expression.exponential(1.5f), Expression.zoom(),
                                Expression.stop(4f, 3f * scale),
                                Expression.stop(10f, 4f * scale),
                                Expression.stop(13f, 6f * scale),
                                Expression.stop(16f, 10f * scale),
                                Expression.stop(19f, 14f * scale),
                                Expression.stop(22f, 18f * scale)
                        )
                ),
                PropertyFactory.lineColor(color)
        )
        mapboxMap.addLayerBelow(routeLayer, "road-label-small")
    }

    fun drawRoute(route: DirectionsRoute, sourceId: String) {
        val featureList = ArrayList<Feature>()
        val lineString = LineString.fromPolyline(route.geometry()!!, Constants.PRECISION_6)
        val coordinates = lineString.coordinates()
        for (i in coordinates.indices) {
            featureList.add(Feature.fromGeometry(LineString.fromLngLats(coordinates)))
        }
        val featureCollection = FeatureCollection.fromFeatures(featureList)
        val source = mapboxMap.getSourceAs<GeoJsonSource>(sourceId)
        source?.setGeoJson(featureCollection)
        boundCameraToRoute(route)
    }

    fun removeAllMarkers() {
        for(markerId in allMarkerIds) {
            removeMarker(markerId)
        }
    }

    /**
     * Draw start marker
     *
     * @param bitmapProvider: a function, that obtains a bitmap and calls a callback on it.
     * this somewhat wierd construction is an attempt have a clean interface while:
     * 1. The MapFragment should not need to know, where to get the bitmap
     * 2. The Bitmap should be loaded only once, even if the marker with the given id is drawn multiple times
     */
    fun drawStartMarker(ohPoint: OHPoint, bitmapProvider:((Bitmap)->Unit)->Unit, color: Int, title: String, id: String) {
        bitmapProvider { bitmap ->
            doDrawStartMarker(ohPoint, bitmap, color, title, id)
        }
    }

    fun doDrawStartMarker(ohPoint: OHPoint, bitmap:Bitmap, color: Int, title: String, id: String) {
        val r = 25F*markerScale
        val frameWidth = 3F*markerScale
        val tipHeight = 30F*markerScale
        val tipWidth = 40F*markerScale
        val scaledBitmap = Bitmap.createScaledBitmap(bitmap, (2 * r).toInt(), (2 * r).toInt(), false);

        val d = (r + frameWidth) * 2
        val output = Bitmap.createBitmap(d.toInt(), (d + tipHeight).toInt(), Bitmap.Config.ARGB_8888);

        // create initial canvas
        val c = Canvas(output)
        c.drawARGB(0, 0, 0, 0)

        // draw background marker circle
        val markerPaint = Paint()
        markerPaint.isAntiAlias = true
        markerPaint.style = Paint.Style.FILL
        c.drawCircle(r + frameWidth, r + frameWidth, r, markerPaint)

        // draw avatar image
        markerPaint.xfermode = PorterDuffXfermode(PorterDuff.Mode.SRC_IN)
        c.drawBitmap(scaledBitmap, frameWidth, frameWidth, markerPaint)

        // draw outline around the circle
        markerPaint.xfermode = null
        markerPaint.style = Paint.Style.STROKE
        markerPaint.color = color
        markerPaint.setStrokeWidth(frameWidth)
        c.drawCircle(r + frameWidth, r + frameWidth, r, markerPaint)

        // draw marker tip
        val topOfTriangle =  sqrt(r.pow(2) - (tipWidth / 2).pow(2)) + r + frameWidth
        val trianglePath = Path();
        trianglePath.moveTo(r - tipWidth / 2 + frameWidth, topOfTriangle)
        trianglePath.lineTo(r + frameWidth, output.height.toFloat())
        trianglePath.lineTo(r + tipWidth / 2 + frameWidth, topOfTriangle)
        val trianglePaint = Paint()
        trianglePaint.xfermode = PorterDuffXfermode(PorterDuff.Mode.DST_OVER)
        trianglePaint.color = color
        trianglePaint.style = Paint.Style.FILL
        c.drawPath(trianglePath, trianglePaint)

        val markerId = "start."+id
        addMarker(ohPoint, markerId, output)
    }

    private fun addMarker(ohPoint: OHPoint, markerId: String, bitmap: Bitmap) {
        val feature = Feature.fromGeometry(ohPoint.toPoint())
        val featureCollection = FeatureCollection.fromFeature(feature)
        val source: Source = GeoJsonSource("source." + markerId, featureCollection)
        removeMarker(markerId)
        mapboxMap.addSource(source)
        mapboxMap.addImage("image." + markerId, bitmap)
        val layer = SymbolLayer("layer." + markerId, "source." + markerId).withProperties(
                iconImage("image." + markerId),
                iconAnchor(ICON_ANCHOR_BOTTOM),
                iconAllowOverlap(true)
        )
        mapboxMap.addLayer(layer)
        allMarkerIds.add(markerId)
    }

    private fun removeMarker(markerId: String) {
        mapboxMap.removeLayer("layer." + markerId)
        mapboxMap.removeImage("image." + markerId)
        mapboxMap.removeSource("source." + markerId)
    }

    fun drawDestinationMarker(ohPoint: OHPoint, color: Int, title: String, id:String) {
        val r = 20F*markerScale
        val tipHeight = 20F*markerScale
        val tipWidth = 30F*markerScale

        val output = Bitmap.createBitmap((2 * r).toInt(), ((2 * r) + tipHeight).toInt(), Bitmap.Config.ARGB_8888);

        // create initial canvas
        val c = Canvas(output)
        c.drawARGB(0, 0, 0, 0)

        // draw background marker circle
        val markerPaint = Paint()
        markerPaint.color = color
        markerPaint.isAntiAlias = true
        markerPaint.style = Paint.Style.FILL
        c.drawCircle(r, r, r, markerPaint)

        if (id=="self") {
            markerPaint.color = Color.WHITE // ColorUtils.blendARGB(color, Color.WHITE, 0.2f)
            c.drawCircle(r, r, r/2, markerPaint)
            markerPaint.color = color
            c.drawCircle(r, r, r/3, markerPaint)

        }

        // draw marker tip
        markerPaint.xfermode = PorterDuffXfermode(PorterDuff.Mode.DST_OVER)
        val topOfTriangle =  sqrt(r.pow(2) - (tipWidth / 2).pow(2)) + r
        val trianglePath = Path();
        trianglePath.moveTo(r - tipWidth / 2, topOfTriangle)
        trianglePath.lineTo(r, output.height.toFloat())
        trianglePath.lineTo(r + tipWidth / 2, topOfTriangle)
        val trianglePaint = Paint()
        trianglePaint.color = color
        trianglePaint.style = Paint.Style.FILL
        c.drawPath(trianglePath, trianglePaint)

        val markerId = "dst."+id
        addMarker(ohPoint,markerId,output)
    }


    private fun initLocationLayer() {
        locationLayer = LocationLayerPlugin(mapView, mapboxMap)
        locationLayer!!.renderMode = RenderMode.COMPASS
        val lastKnownLocation = lastKnownLocation
        if (lastKnownLocation == null) {
            // show all of Germany
            animateCameraBbox(LatLngBounds.from(55.2, 5.8, 47.3, 15.0), CAMERA_ANIMATION_DURATION)
        } else {
            animateCamera(LatLng(lastKnownLocation.latitude, lastKnownLocation.longitude))
        }
        locationLayer!!.locationEngine!!.addLocationEngineListener(myLocationEngineListener())
    }

    fun hideLoading() {
        if (loading.visibility == View.VISIBLE) {
            loading.visibility = View.INVISIBLE
        }
    }

    fun showLoading() {
        if (loading.visibility == View.INVISIBLE) {
            loading.visibility = View.VISIBLE
        }
    }

    fun boundCameraToRoute(route: DirectionsRoute) {
        val routeCoords = LineString.fromPolyline(
                route.geometry()!!,
                Constants.PRECISION_6
        ).coordinates()
        val bboxPoints = ArrayList<LatLng>()
        for (point in routeCoords) {
            bboxPoints.add(LatLng(point.latitude(), point.longitude()))
        }
        if (bboxPoints.size > 1) {
            try {
                val bounds = LatLngBounds.Builder().includes(bboxPoints).build()
                // left, top, right, bottom
                animateCameraBbox(bounds, CAMERA_ANIMATION_DURATION)
            } catch (exception: InvalidLatLngBoundsException) {
                Toast.makeText(context, R.string.error_valid_route_not_found, Toast.LENGTH_SHORT).show()
            }

        }
    }

    fun animateCameraBbox(bounds: LatLngBounds, animationTime: Int) {
        val position = mapboxMap.getCameraForLatLngBounds(bounds, padding)
        mapboxMap.animateCamera(CameraUpdateFactory.newCameraPosition(position), animationTime)
    }

    fun animateCamera(point: LatLng) {
        mapboxMap.animateCamera(
                CameraUpdateFactory.newLatLngZoom(point, DEFAULT_CAMERA_ZOOM.toDouble()),
                CAMERA_ANIMATION_DURATION
        )
    }

    @OnClick(R.id.current_location_fab)
    fun onCurrentLocationButtonClick() {
        lastKnownLocation?.let {
            animateCamera(LatLng(it.latitude, it.longitude))
        } ?: run {
            var stringId = if (!PermissionsManager.areLocationPermissionsGranted(context)) {
                R.string.no_location_permission
            } else {
                R.string.current_location_unknown
            }
            Snackbar.make(mapView, stringId, Snackbar.LENGTH_LONG).show()
        }
    }

    inner class myLocationEngineListener : LocationEngineListener {
        @RequiresApi(Build.VERSION_CODES.M)
        override fun onLocationChanged(location: Location?) {
            if (location == null) {
                currentLocationFab.setImageResource(R.drawable.ic_current_location_inactive_24)
            } else {
                currentLocationFab.setImageResource(R.drawable.ic_current_location_24)
            }
        }

        override fun onConnected() {
            //
        }

    }

    override fun onStart() {
        super.onStart()
        mapView.onStart()
        locationLayer?.onStart()
    }

    override fun onResume() {
        super.onResume()
        mapView.onResume()
    }

    override fun onPause() {
        super.onPause()
        mapView.onPause()
    }

    override fun onLowMemory() {
        super.onLowMemory()
        mapView.onLowMemory()
    }

    override fun onStop() {
        super.onStop()
        mapView.onStop()
        locationLayer?.onStop()
    }

    override fun onDestroyView() {
        mapView.onDestroy()
        super.onDestroyView()
    }

    override fun onSaveInstanceState(outState: Bundle) {
        super.onSaveInstanceState(outState)
        mapView.onSaveInstanceState(outState)
    }

    @CallSuper
    override fun onMapReady(mapboxMap: MapboxMap) {
        allMarkerIds = ArrayList()
        this.mapboxMap = mapboxMap
        this.mapboxMap.uiSettings.attributionDialogManager =
            GHAttributionDialogManager(this.mapView.context, this.mapboxMap)
        addRouteLayer(
                PASSENGER_ROUTE_LAYER_ID,
                PASSENGER_ROUTE_SOURCE_ID,
                ContextCompat.getColor(requireContext(), R.color.passengerRouteColor)
        )
        addRouteLayer(
                DRIVER_ROUTE_LAYER_ID,
                DRIVER_ROUTE_SOURCE_ID,
                ContextCompat.getColor(requireContext(), R.color.driverRouteColor)
        )

        checkLocationPermissions()

        externalOnMapReadyCallback?.onMapReady(mapboxMap)
    }

    fun setOnMapReadyCallback(callback: OnMapReadyCallback) {
        externalOnMapReadyCallback = callback
    }

    private val REQUEST_PERMISSIONS_CODE = 0

    private fun checkLocationPermissions() {
        if (areLocationPermissionsGranted()) {
            initLocationLayer()
        } else {
            val permissions = arrayOf(Manifest.permission.ACCESS_FINE_LOCATION)
            if (shouldShowRequestPermissionRationale(Manifest.permission.ACCESS_FINE_LOCATION)) {
                Toast.makeText(
                        context, R.string.need_location_permission,
                        Toast.LENGTH_LONG
                ).show()
            }
            requestPermissions(permissions, REQUEST_PERMISSIONS_CODE)
        }
    }

    override fun onRequestPermissionsResult(
            requestCode: Int, permissions: Array<String>,
            grantResults: IntArray
    ) {
        if (requestCode!=REQUEST_PERMISSIONS_CODE) {
            return
        }
        val granted = grantResults.size > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED
        if (granted) {
            initLocationLayer()
        } else {
            Toast.makeText(
                    context, getString(R.string.no_location_permission),
                    Toast.LENGTH_LONG
            ).show()
        }

    }

    private fun isPermissionGranted(permission: String): Boolean {
        return (ContextCompat.checkSelfPermission(requireContext(), permission)
                === PackageManager.PERMISSION_GRANTED)
    }

    open fun areLocationPermissionsGranted(): Boolean {
        return (isPermissionGranted(Manifest.permission.ACCESS_COARSE_LOCATION)
                || isPermissionGranted(Manifest.permission.ACCESS_FINE_LOCATION))
    }




    companion object {
        val DRIVER_ROUTE_SOURCE_ID = "driver-route-source"
        val PASSENGER_ROUTE_SOURCE_ID = "passenger-route-source"

        private val DEFAULT_CAMERA_ZOOM = 16
        private val DRIVER_ROUTE_LAYER_ID = "driver-route-layer"
        private val PASSENGER_ROUTE_LAYER_ID = "passenger-route-layer"
    }
}
