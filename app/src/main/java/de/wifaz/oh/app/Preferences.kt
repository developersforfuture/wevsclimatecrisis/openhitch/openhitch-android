package de.wifaz.oh.app

import android.content.Context
import androidx.preference.PreferenceManager
import de.wifaz.oh.app.R
import com.mapbox.services.android.navigation.v5.utils.LocaleUtils
import java.util.*

object Preferences {
    fun getUnitType(context: Context): String? {
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        val defaultUnitType = context.getString(R.string.default_unit_type)
        var unitType = sharedPreferences.getString(context.getString(R.string.unit_type_key), defaultUnitType)
        if (unitType == defaultUnitType) {
            unitType = LocaleUtils().getUnitTypeForDeviceLocale(context)
        }

        return unitType
    }

    fun getLocale(context: Context): Locale {
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)
        val defaultLanguage = context.getString(R.string.default_locale)
        val language = sharedPreferences.getString(context.getString(R.string.language_key), null) ?: defaultLanguage
        return if (language == defaultLanguage) {
            LocaleUtils().inferDeviceLocale(context)
        } else {
            Locale(language)
        }
    }

}
