package de.wifaz.oh.app

import android.app.ActionBar
import android.app.AlertDialog
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.text.Html
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.InputMethodManager
import android.widget.TextView
import androidx.annotation.CallSuper
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.preference.PreferenceManager
import com.google.android.material.snackbar.Snackbar
import de.wifaz.oh.api.Api
import de.wifaz.oh.api.Errors
import io.reactivex.disposables.CompositeDisposable

class HitchAppError(message: String) : RuntimeException(message)

open class HitchActivity : AppCompatActivity(), HitchComponent {
    val FIRST_START_DIALOG_VERSION = 1
    val CHANGE_SETTING_REQUEST_CODE = 1001

    override val api: Api
        get() = (application as HitchApplication).api
    override val disposables = CompositeDisposable()

    val errors: Errors
        get() = api.errors


    @CallSuper
    override fun onStart() {
        super.onStart()
        observe(api.errors.errorStream(), ::showError)
    }

    fun showError(error: Errors.Error) {
        // get root view (cf. https://stackoverflow.com/questions/4486034/get-root-view-from-current-activity#5069354 )
        val rootView = (findViewById<View>(android.R.id.content) as ViewGroup).getChildAt(0) as View
        if (error.type != Errors.Type.NONFATAL) {
            Snackbar.make(rootView, error.message, Snackbar.LENGTH_LONG).show()
        }
    }

    @CallSuper
    override fun onStop() {
        disposables.clear()
        super.onStop()
    }

    var showActionBarMenu: Boolean = true

    fun initActionBar(title: String = "", showMenu: Boolean = true) {
        val mainToolbar = findViewById<Toolbar>(R.id.main_toolbar)
        showActionBarMenu = showMenu
        setSupportActionBar(mainToolbar)
        val supportActionBar = supportActionBar!!
        supportActionBar.displayOptions = ActionBar.DISPLAY_SHOW_CUSTOM
        supportActionBar.setCustomView(R.layout.titlebar)
        setActionBarTitle(title)
    }

    fun setActionBarTitle(title: String) {
        val supportActionBar = supportActionBar!!
        val customView = supportActionBar.customView
        val titleView = customView.findViewById<TextView>(R.id.action_bar_title)
        titleView.text = title
    }

    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        if (showActionBarMenu) {
            // Inflate the menu; this adds items to the action bar if it is present.
            menuInflater.inflate(R.menu.action_bar_menu, menu)
        }
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when (item.itemId) {
            R.id.feedback -> {
                startFeedback()
                return true
            }
            R.id.settings -> {
                showSettings()
                return true
            }
            R.id.help -> {
                showHelpDialog()
                return true
            }
            else -> return super.onOptionsItemSelected(item)
        }
    }

    private fun startFeedback() {
        startActivity(Intent(this, CreateFeedbackActivity::class.java))
    }

    private fun showSettings() {
        startActivityForResult(Intent(this, HitchSettingsActivity::class.java), CHANGE_SETTING_REQUEST_CODE)
    }

    protected fun showHelpDialog() {
        val builder = AlertDialog.Builder(this)
        builder.setTitle(R.string.help_message_title)
        builder.setMessage(Html.fromHtml("1. Please note, this is a demo and not a full featured application. The purpose of this application is to provide an easy starting ohPoint for developers to create a navigation application with GraphHopper<br/>2. You should enable your GPS/location<br/>3.You can either search for a location using the magnifier icon or by long pressing on the map<br/>4. Start the navigation by tapping the arrow button<br/><br/>This project is 100% open source, contributions are welcome.<br/><br/>Please drive carefully and always abide local law and signs. Roads might be impassible due to construction projects, traffic, weather, or other events."))
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this)
        builder.setPositiveButton(R.string.agree) { _, _ ->
            val editor = sharedPreferences.edit()
            editor.putInt(getString(R.string.first_start_dialog_key), FIRST_START_DIALOG_VERSION)
            editor.apply()
        }
        builder.setNeutralButton(R.string.github) { _, _ ->
            startActivity(
                Intent(
                    Intent.ACTION_VIEW,
                    Uri.parse("https://github.com/graphhopper/graphhopper-navigation-example")
                )
            )
        }

        val dialog = builder.create()
        dialog.show()
    }

    fun hideKeyboard() {
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.hideSoftInputFromWindow(findViewById<View>(android.R.id.content).windowToken, 0)
    }

}
