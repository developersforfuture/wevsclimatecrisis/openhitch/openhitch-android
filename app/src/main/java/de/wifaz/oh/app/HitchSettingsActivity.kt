package de.wifaz.oh.app

import android.content.Intent
import android.content.SharedPreferences
import android.content.SharedPreferences.OnSharedPreferenceChangeListener
import android.os.Bundle
import androidx.preference.Preference
import androidx.preference.PreferenceFragmentCompat
import androidx.preference.PreferenceManager
import de.wifaz.oh.app.R
import com.mapbox.api.directions.v5.DirectionsCriteria
import de.wifaz.oh.app.ProfilePictureActivity.Companion.MODE_SETTINGS
import de.wifaz.oh.app.ProfilePictureActivity.Companion.PARAMETER_MODE
import java.util.Locale

class HitchSettingsActivity : HitchActivity() {
    private var listener: OnSharedPreferenceChangeListener? = null
    public override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        listener = OnSharedPreferenceChangeListener { _: SharedPreferences?, key: String ->
            val resultIntent = Intent()
            if (key==getString(R.string.unit_type_key)) {
                api.settings.unitType = Preferences.getUnitType(this) ?: DirectionsCriteria.METRIC
                resultIntent.putExtra(UNIT_TYPE_CHANGED, true)
            }
            if (key==getString(R.string.language_key)) {
                api.settings.locale = Preferences.getLocale(this)
                resultIntent.putExtra(LANGUAGE_CHANGED, true)
            }
            setResult(RESULT_OK, resultIntent)
        }
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this)
        sharedPreferences.registerOnSharedPreferenceChangeListener(listener)
        supportFragmentManager.beginTransaction().replace(
            android.R.id.content, HitchPreferenceFragment()
        ).commit()
    }

    class HitchPreferenceFragment : PreferenceFragmentCompat() {
        override fun onCreate(savedInstanceState: Bundle?) {
            super.onCreate(savedInstanceState)
            addPreferencesFromResource(R.xml.fragment_navigation_view_preferences)
            PreferenceManager.setDefaultValues(activity, R.xml.fragment_navigation_view_preferences, false)
            val myPref = findPreference<Preference>(getString(R.string.account_key))
            myPref!!.onPreferenceClickListener = Preference.OnPreferenceClickListener {
                startActivity(Intent(activity, AccountActivity::class.java))
                true
            }
            findPreference<Preference>(getString(R.string.profile_picture_key))!!.onPreferenceClickListener =
                Preference.OnPreferenceClickListener {
                    val intent = Intent(activity, ProfilePictureActivity::class.java)
                    intent.putExtra(PARAMETER_MODE, MODE_SETTINGS)
                    startActivity(intent)
                    true
                }
            findPreference<Preference>(getString(R.string.simulate_route_key))!!.isVisible = BuildConfig.DEBUG
        }

        override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {}
    }

    companion object {
        const val UNIT_TYPE_CHANGED = "unit_type_changed"
        const val LANGUAGE_CHANGED = "language_changed"
        const val PROFILE_CHANGED = "profile_changed"
    }
}