package de.wifaz.oh.app

import android.content.Intent
import android.os.Bundle
import butterknife.ButterKnife
import de.wifaz.oh.app.R

class LauncherActivity : HitchActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_launcher)
        ButterKnife.bind(this)

        ////
        // Currently, the HitchService doesn't do anything, so we don't start it.
        // Later, we want to listen for messages from the server, even when the app is
        // not running (see issue #40). Then we will activate this again.
        ////
        // startService(Intent(this, HitchService::class.java))

        launch()
    }

    fun launch() {
        if (api.account.isFullyRegistered()) {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
            finish()
        } else {
            val intent = Intent(this, RegistrationActivity::class.java)
            startActivity(intent)
            finish()
        }
    }
}