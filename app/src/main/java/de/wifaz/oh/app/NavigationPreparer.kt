package de.wifaz.oh.app

import android.app.AlertDialog
import android.location.Location
import android.text.Html
import android.view.View
import androidx.preference.PreferenceManager
import de.wifaz.oh.app.R
import com.mapbox.api.directions.v5.models.DirectionsRoute
import com.mapbox.services.android.navigation.ui.v5.NavigationLauncher
import com.mapbox.services.android.navigation.ui.v5.NavigationLauncherOptions
import de.wifaz.oh.protocol.OHPoint

// FIXME need interface for this

class NavigationPreparer(
    val activity: HitchActivity,
    val finishCurrentActivity: Boolean
) {
    var view = activity.findViewById<View>(android.R.id.content)  // FIXME need nicer interface

    fun launchNavigation(waypoints: List<OHPoint>, checkLocation: Boolean) {
        val lastknownlocation: Location? = null // FIXME set if  checkLocation=true. But where do I get it?
        activity.api.fetchRoute(waypoints, {}, ::launchNavigationWithRoute)
    }

    fun launchNavigationWithRoute(route: DirectionsRoute) {
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(activity)
        if (sharedPreferences.getInt(
                activity.getString(R.string.first_navigation_dialog_key),
                -1
            ) < FIRST_NAVIGATION_DIALOG_VERSION
        ) {
            showNavigationDialog(route)
            return
        }

        _launchNavigationWithRoute(route)

        /*
        ** FIMXE the following code checks the current location and the bearing,
        ** to be shure to start navigation at the right place and into the right location
        ** To make it work, we need to have access two things from here: The current location with bearing
        ** and the ability to fetch a new route
        **
        Location lastKnownLocation = FIXME

        List<OHPoint> waypoints = parameters.waypoints;
        if (lastKnownLocation != null && waypoints!=null) {
            float[] distance = new float[1];
            Location.distanceBetween(lastKnownLocation.getLatitude(), lastKnownLocation.getLongitude(), waypoints.get(0).getLatitude(), waypoints.get(0).getLongitude(), distance);

            //Ask the user if he would like to recalculate the route from his current positions

            if (distance[0] > 100) {
                AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle(R.string.error_too_far_from_start_title);
                builder.setMessage(R.string.error_too_far_from_start_message);
                builder.setPositiveButton(R.string.ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        waypoints.set(0, OHPoint.fromLngLat(lastKnownLocation.getLongitude(), lastKnownLocation.getLatitude()));

                        // FIXME
                        // mapFragment.fetchRoute(Util.ohPoints2points(waypoints),true,ChooseWaypointsActivity.this);
                    }
                });
                builder.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        _launchNavigationWithRoute();
                    }
                });

                AlertDialog dialog = builder.create();
                dialog.show();
            } else {
                _launchNavigationWithRoute();
            }
        } else {
            _launchNavigationWithRoute();
        }
        */
    }

    private fun showNavigationDialog(route: DirectionsRoute) {
        val builder = AlertDialog.Builder(activity)
        builder.setTitle(R.string.legal_title)
        builder.setMessage(Html.fromHtml("You are required to comply with applicable laws.<br/>When using mapping data, directions or other data from GraphHopper / OpenStreetMap, it is possible that the results differ from the actual situation. You should therefore act at your own discretion. Use of GraphHopper / OpenStreetMap is at your own risk. You are responsible for your own behavior and consequences at all times."))
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(activity)
        builder.setPositiveButton(R.string.agree) { _, _ ->
            val editor = sharedPreferences.edit()
            editor.putInt(activity.getString(R.string.first_navigation_dialog_key), FIRST_NAVIGATION_DIALOG_VERSION)
            editor.apply()

            launchNavigationWithRoute(route)
        }

        val dialog = builder.create()
        dialog.show()
    }


    private fun _launchNavigationWithRoute(route: DirectionsRoute) {
        val optionsBuilder = NavigationLauncherOptions.builder()
            .shouldSimulateRoute(shouldSimulateRouteFromSharedPreferences)
            .directionsProfile("driving")
            .waynameChipEnabled(false)

        optionsBuilder.directionsRoute(route)

        NavigationLauncher.startNavigation(activity, optionsBuilder.build())
        if (finishCurrentActivity) {
            activity.finish()
        }
    }

    private val shouldSimulateRouteFromSharedPreferences: Boolean
        get() {
            val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(activity)
            return sharedPreferences.getBoolean(activity.getString(R.string.simulate_route_key), false)
        }

    companion object {
        private val FIRST_NAVIGATION_DIALOG_VERSION = 1
    }

}