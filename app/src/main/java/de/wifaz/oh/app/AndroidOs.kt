package de.wifaz.oh.app

import android.content.Context
import de.wifaz.oh.api.Os
import java.io.File

class AndroidOs(private val context: Context) : Os {
    private val PREFERENCES_NAME = "de.wifaz.oh.api_preferences"

    override val cacheDirectory: File
        get() = context.cacheDir

    override fun load(key: String): String? {
        val preferences = context.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE)
        return preferences.getString(key, null)
    }

    override fun save(key: String, value: String) {
        val editor = context.getSharedPreferences(PREFERENCES_NAME, Context.MODE_PRIVATE).edit()
        editor.putString(key, value)
        editor.commit()
    }
}