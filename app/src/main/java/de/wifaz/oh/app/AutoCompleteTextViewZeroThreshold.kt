package de.wifaz.oh.app

import android.content.Context
import android.util.AttributeSet

// cf. https://stackoverflow.com/questions/2126717/android-autocompletetextview-show-suggestions-when-no-text-entered#2126852

class AutoCompleteTextViewZeroThreshold : com.google.android.material.textfield.MaterialAutoCompleteTextView {
    constructor(context: Context, attrs: AttributeSet?) : super(context, attrs)
    constructor(context: Context) : super(context)

    override fun enoughToFilter(): Boolean {
        return true
    }
}