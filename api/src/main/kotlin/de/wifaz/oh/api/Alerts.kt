package de.wifaz.oh.app.service

import de.wifaz.oh.api.AlertSummaryItem
import io.reactivex.Observable
import io.reactivex.Observer
import io.reactivex.disposables.Disposable
import kotlin.reflect.KClass


class Alerts {
    internal val alertListStore = object : ListStore<Alert>() {
        override fun getKey(x: Alert): String {
            return x.getKey()
        }
    }

    @Synchronized
    fun put(alert: Alert) {
        alertListStore.put(alert)
    }

    @Synchronized
    fun remove(key: String) {
        alertListStore.remove(key)
    }

    fun alertListStream(predicate: (x: Alert) -> Boolean): Observable<List<Alert>> {
        return alertListStore.listStream(predicate)
    }

    fun alertSummaryStream(): Observable<List<AlertSummaryItem>> {
        return object : Observable<List<AlertSummaryItem>>() {
            override fun subscribeActual(observer: Observer<in List<AlertSummaryItem>>) {
                alertListStream({ true }).subscribe(AlertSummaryOperator(observer))
            }

        }
    }

    internal fun erase() {
        alertListStore.erase()
    }

    private inner class AlertSummaryOperator(private val observer: Observer<in List<AlertSummaryItem>>) :
        Observer<List<Alert>> {
        override fun onComplete() {
            observer.onComplete()
        }

        override fun onSubscribe(d: Disposable) {
            observer.onSubscribe(d)
        }

        override fun onNext(t: List<Alert>) {
            val newItems = HashMap<KClass<out Alert>, AlertSummaryItem>()
            // val newItems = EnumMap<AlertType, AlertSummaryItem>(AlertType::class.java)
            for (alert in t) {
                var item: AlertSummaryItem? = newItems.get(alert::class)
                if (item == null) {
                    item = AlertSummaryItem(alert)
                    newItems.put(alert::class, item)
                } else {
                    item.add(alert)
                }
            }
            observer.onNext(newItems.values.toList())
        }

        override fun onError(e: Throwable) {
            observer.onError(e)
        }

    }
}