package de.wifaz.oh.api

import java.io.File

interface Os {
    val cacheDirectory: File

    fun load(key: String): String?
    fun save(key: String, value: String)
}