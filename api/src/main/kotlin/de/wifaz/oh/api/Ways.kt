package de.wifaz.oh.app.service

import de.wifaz.oh.api.Api
import de.wifaz.oh.api.HitchApiError
import de.wifaz.oh.api.HitchApiReferenceException
import de.wifaz.oh.protocol.Lift
import de.wifaz.oh.protocol.Lift.Status.*
import de.wifaz.oh.protocol.LiftInfo
import de.wifaz.oh.protocol.OHPoint
import de.wifaz.oh.protocol.Role.DRIVER
import de.wifaz.oh.protocol.Role.PASSENGER
import de.wifaz.oh.protocol.Way
import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject
import java.math.BigDecimal
import java.util.*

class Ways(private val api: Api) {
    private val errors = api.errors

    private val wayContextListStore = object : ListStore<WayContext>() {
        override fun getKey(x: WayContext): String {
            return x.way.id
        }
    }

    fun wayListStream(predicate: (way: Way) -> Boolean = { true }): Observable<List<Way>> {
        val p: (wayContext: WayContext) -> Boolean = {
            predicate(it.way)
        }
        return wayContextListStore.listStream(p).map { wayContextList ->
            wayContextList.map { it.way }
        }
    }

    fun liftInfoStream(way_id: String, lift_id: String): Observable<LiftInfo> {
        return getWayContext(way_id).liftInfoListStore.itemStream(lift_id)
    }


    fun wayStream(way_id: String): Observable<Way> {
        return wayContextListStore.itemStream(way_id).map { wayContext: WayContext ->
            wayContext.way
        }
    }

    // XXX beware no updates
    fun getWay(way_id: String): Way {
        return getWayContext(way_id).way
    }

    private fun getWayContext(way_id: String): WayContext {
        return wayContextListStore.getItem(way_id)
            ?: throw HitchApiReferenceException("getWayContext with unknown id: " + way_id)
    }

    fun putWay(way: Way) {
        validateWay(way)
        wayContextListStore.put(WayContext(api, way))
    }

    private fun validateWay(way: Way) {
        way.waypoints.size >= 2 || throw HitchApiError("Not enough waypoints: " + way.waypoints.size)
    }

    /**
     * Change the status of the way in the local database.
     * It is checked if the status change is legal. When the status
     * is changed to CANCELLED, the way is removed from the local database.
     *
     * No server interaction is performed. GUI-code should use {@link Api.changeWayStatus} to
     * make the status change also known to the server.
     */
    internal fun changeWayStatus(wayId: String, newStatus: Way.Status) {
        val wayContext = getWayContext(wayId)
        if (newStatus == Way.Status.CANCELED) {
            wayContextListStore.remove(wayId)
        } else {
            wayContext.way = wayContext.way.copy(status = newStatus)
            // Put empty map to trigger publishing
            wayContextListStore.put(wayContext)
        }
    }

    fun liftInfoListStream(
        way_id: String,
        states: EnumSet<Lift.Status> = EnumSet.allOf(Lift.Status::class.java)
    ): Observable<List<LiftInfo>> {
        val wayContext = getWayContext(way_id)
        return wayContext.liftInfoListStore.listStream { it.lift.status in states }
    }

    fun liftInfoCountStream(
        way_id: String,
        states: EnumSet<Lift.Status> = EnumSet.allOf(Lift.Status::class.java)
    ): Observable<Int> {
        return liftInfoListStream(way_id, states)
            .map { it.size }
    }

    fun liftInfoCountMapStream(way_id: String): Observable<Map<Lift.Status, Int?>> {
        return liftInfoListStream(way_id, EnumSet.allOf(Lift.Status::class.java))
            .map { mapCount(it) }
    }

    private fun mapCount(lift_infos: List<LiftInfo>): Map<Lift.Status, Int?> {
        val result = EnumMap<Lift.Status, Int>(Lift.Status::class.java)

        for (liftInfo in lift_infos) {
            val status = liftInfo.lift.status
            result.put(status, (result.get(status) ?: 0) + 1)
        }
        return result
    }

    fun collisionListStream(wayId: String): Observable<List<Way>> {
        val way = getWayContext(wayId).way
        // FIXME make shure that any Fragment using collisionListStream is closed/reloaded on chonge of way status or timing
        return if (way.status in Way.collisionRelevantStates) {
            wayListStream {
                it.id != wayId && it.status in Way.collisionRelevantStates && doCollide(way, it)
            }
        } else {
            Observable.empty()
        }
    }

    fun collisionListStream(startTime: Long, endTime: Long): Observable<List<Way>> {
        return wayListStream {
            it.status in Way.collisionRelevantStates && doCollide(startTime, endTime, it)
        }
    }

    fun getLiftInfo(way_id: String, lift_id: String): LiftInfo {
        val wayContext = getWayContext(way_id)
        return wayContext.liftInfoListStore.getItem(lift_id)
            ?: throw HitchApiReferenceException("getLiftInfo with unknown lift_id $lift_id from way ${way_id}")
    }

    /**
     * Change the status of the lift in the local database.
     * It is checked if the status change is legal.
     *
     * No server interaction is performed. GUI-code should use {@link Api.changeLiftStatus} to
     * make the status change also known the server.
     */
    internal fun changeLiftStatus(
        way_id: String,
        lift_id: String,
        newStatus: Lift.Status
    ) {
        var liftInfo = getLiftInfo(way_id, lift_id)
        val newLift = liftInfo.lift.copy(status = newStatus)
        val newLiftInfo = liftInfo.copy(lift = newLift)
        putLiftInfo(way_id, newLiftInfo)
    }

    internal fun putLiftInfo(way_id: String, liftInfo: LiftInfo) {
        getWayContext(way_id).liftInfoListStore.put(liftInfo)
    }

    fun cancelLift(wayId: String, liftId: String) {
        val liftInfo: LiftInfo = getLiftInfo(wayId, liftId)
        if (liftInfo.lift.status != SUGGESTED) {
            var newStatus = when (liftInfo.role) {
                DRIVER -> DRIVER_CANCELED
                PASSENGER -> PASSENGER_CANCELED
            }
            changeLiftStatus(wayId, liftId, newStatus)
        }
    }

    internal fun putLiftInfos(reference_way_id: String, lift_infos: List<LiftInfo>) {
        validateLiftInfos(lift_infos, reference_way_id)
        getWayContext(reference_way_id).liftInfoListStore.putList(lift_infos)
    }

    private fun validateLiftInfos(lift_infos: List<LiftInfo>, reference_way_id: String) {
        for (liftInfo in lift_infos) {
            if (reference_way_id != liftInfo.reference_way_id()) {
                throw HitchApiError(
                    String.format(
                        "Received LiftInfo refers to wrong way: %s instead of %s",
                        liftInfo.reference_way_id(),
                        reference_way_id
                    )
                )
            }
        }
    }

    internal fun removeWay(wayId: String) {
        wayContextListStore.remove(wayId)
    }

    internal fun erase() {
        wayContextListStore.erase()
    }
}

fun doCollide(w1: Way, w2: Way): Boolean = w1.end_time > w2.start_time && w1.start_time < w2.end_time

fun doCollide(startTime: Long, endTime: Long, w: Way): Boolean = endTime > w.start_time && startTime < w.end_time

private class WayContext(private val api: Api, var way: Way) {
    val isLoadedSubject: BehaviorSubject<Boolean> = BehaviorSubject.create<Boolean>().apply { onNext(false) }
    val liftInfoListStore = object : ListStore<LiftInfo>() {
        override fun getKey(x: LiftInfo): String {
            return x.lift.id
        }

        override fun internalPut(key: String, x: LiftInfo?) {
            super.internalPut(key, x)
            val liftInfo = x
            when (way.role) {
                DRIVER -> {
                    if (liftInfo?.lift?.status == REQUESTED) {
                        val alert = Alert.Request(way.id, liftInfo.lift.id)
                        api.alerts.put(alert)
                    } else {
                        // FIXME should have alert type in key somehow
                        api.alerts.remove(key)
                    }
                }
                PASSENGER -> {
                    if (liftInfo?.lift?.status == SUGGESTED) {
                        val alert = Alert.Offer(way.id, liftInfo.lift.id)
                        api.alerts.put(alert)
                    } else {
                        api.alerts.remove(key)
                    }
                }
            }
        }
    }


}



