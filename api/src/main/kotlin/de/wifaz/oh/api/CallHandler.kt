package de.wifaz.oh.api

import de.wifaz.oh.api.Errors.Type.AUTHENTICATION
import de.wifaz.oh.api.Errors.Type.CONNECTIVITY
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.net.ConnectException

class CallHandler(val errors: Errors) {
    /**
     * Enqueues the given call using the given callback
     *
     * The success callback is called when the call succeeded and the result is given to the
     * callback.
     *
     * @param call the call to enqueue
     * @param description a String describing the call used for logging and error messages
     * @param onSuccess the callback used when the call succeeded.
     */
    fun <T> enqueueCall(call: Call<T>, description: String, onError: () -> Unit = {}, onSuccess: (T) -> Unit = {}) {
        // FIXME allow HitchMessage directly in response
        // Timber.d("$description:_$call")
        call.enqueue(object : Callback<T> {

            override fun onResponse(call: Call<T>, response: Response<T>) {
                if (response.isSuccessful) {
                    // Timber.d("Call successful (%s)", description)
                    val body = response.body()!!
                    onSuccess(body)
                } else {
                    onError()
                    when (response.code()) {
                        401 -> errors.error(
                            AUTHENTICATION,
                            "Authentication failed",
                            "call: ${description}, message: ${response.message()}"
                        )
                        else -> errors.unexpected(
                            "Unsuccessful call",
                            "call: ${description}, status: ${response.code()}, message: ${response.message()}, url: ${call.request().url()}, body: ${call.request().body()}"
                        )
                    }
                }
            }

            override fun onFailure(call: Call<T>, t: Throwable) {
                if (t is ConnectException) {
                    onError()
                    errors.error(CONNECTIVITY, "Server connection failed for call", "call: ${description}, t")
                } else {
                    onError()
                    errors.unexpected("Call failed", "call: ${description}, t")
                }
            }
        })
    }

}
