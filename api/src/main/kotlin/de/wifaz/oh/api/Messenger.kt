package de.wifaz.oh.api

import de.wifaz.oh.protocol.HitchMessage
import io.reactivex.Observable

interface Messenger {
    fun messageStream(account: Account): Observable<HitchMessage>
}