package de.wifaz.oh.app.service

import de.wifaz.oh.protocol.LiftInfo
import de.wifaz.oh.protocol.Role

fun LiftInfo.reference_way_id(): String {
    return when (this.role) {
        Role.DRIVER -> this.lift.driver_way_id
        Role.PASSENGER -> this.lift.passenger_way_id
    }
}
