package de.wifaz.oh.api

import com.fasterxml.jackson.core.type.TypeReference
import com.fasterxml.jackson.databind.ObjectMapper
import java.io.IOException

class Storage(private val os: Os, private val errors: Errors) {
    fun <T> load(key: String, typeReference: TypeReference<T>): T? {
        val json = os.load(key)
        if (json == null) {
            return null
        }
        val mapper = ObjectMapper()
        try {
            var result: T = mapper.readValue<T>(json, typeReference)
            return result
            // val collectionType = mapper.typeFactory.constructCollectionType(List<*>::class.java!!, OHPoint::class.java!!)
            // favorites = mapper.readValue<List<OHPoint>>(json, collectionType)
        } catch (e: Exception) {
            when (e) {
                is IOException, is ClassCastException -> {
                    errors.unexpected(
                        "Could not convert stored json to java object",
                        "ignoring unreadable stored value. Key: $key, json: $json",
                        e
                    )
                }
                else -> {
                    throw e
                }
            }
            return null
        }
    }

    fun <T> save(key: String, value: T?) {
        val mapper = ObjectMapper()
        val json = mapper.writeValueAsString(value)
        os.save(key, json)
    }

}