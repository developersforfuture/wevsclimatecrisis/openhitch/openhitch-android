package de.wifaz.oh.api

import com.fasterxml.jackson.core.type.TypeReference
import de.wifaz.oh.protocol.HitchInterface
import de.wifaz.oh.protocol.RegistrationState
import de.wifaz.oh.protocol.User
import io.reactivex.Observable
import io.reactivex.subjects.BehaviorSubject
import okhttp3.MediaType
import okhttp3.RequestBody
import java.io.File


/**
 * XXX null means not registered
 */
class Account(
    private val hitchInterface: HitchInterface,
    internal val storage: Storage,
    internal val errors: Errors,
    private val callHandler: CallHandler
) {
    private val userContext = UserContext(this)
    val user
        get() = userContext.user
    val registrationState
        get() = userContext.registrationState
    val token
        get() = userContext.token

    fun isAutheticated(): Boolean {
        return userContext.isAuthenticated()
    }

    fun isFullyRegistered(): Boolean {
        return userContext.isFullyRegistered()
    }

    fun setUser(user: User?, registrationState: RegistrationState?, token: String?) {
        userContext.set(user, registrationState, token)
    }


    internal val isAuthenticatedSubject: BehaviorSubject<Boolean> = BehaviorSubject.create()

    fun isAuthenticatedStream(): Observable<Boolean> = isAuthenticatedSubject

    init {
        userContext.load()
        isAuthenticatedSubject.onNext(userContext.isAuthenticated())
    }

    fun register(newUser: User, onBaseRegistrationComplete: () -> Unit) {
        val call = hitchInterface.createUser(newUser)
        callHandler.enqueueCall(call, "Registration") { responseBody ->
            val token = responseBody.token
            val registrationState = responseBody.registration_state
            userContext.set(newUser, registrationState, token)
            if (isAutheticated()) {
                onBaseRegistrationComplete()
            }
        }
    }

    fun updateUser(newUser: User, onUpdateUserComplete: () -> Unit) {
        val call = hitchInterface.updateUser(newUser.id, newUser)
        callHandler.enqueueCall(call, "Update user") {
            userContext.updateUser(newUser)
            onUpdateUserComplete()
        }
    }

    internal fun updateUserData(newUser: User) {
        userContext.updateUser(newUser)
    }

    fun uploadProfilePicture(imageFile: File, onUploadComplete: () -> Unit) {
        val userId = userContext.user?.id ?: return
        val profilePicture = RequestBody.create(MediaType.parse("image/jpeg"), imageFile.readBytes())

        val call = hitchInterface.uploadProfilePicture(userId, profilePicture)
        callHandler.enqueueCall(call, "Upload profile picture") { newRegistrationState ->
            if (!newRegistrationState.fully_registered) {
                HitchApiError("Inconsitent registration state: not fully registered after successful profile picture upload. registrationsState: ${newRegistrationState}")
            }
            userContext.updateRegistrationState(newRegistrationState)
            onUploadComplete()
        }
    }


}

private class UserContext(val account: Account) {
    var user: User? = null
        private set
    var registrationState: RegistrationState? = null
        private set
    var token: String? = null
        private set

    fun set(user: User?, registrationState: RegistrationState?, token: String?) {
        var oldUser = this.user
        if (user!=null && oldUser!=null && user.id != oldUser.id) {
            // if changing from one user to another broadcast logoff first (e.g. to trigger new MQTT-subscription)
            account.isAuthenticatedSubject.onNext(false)
        }
        setVariables(user, registrationState, token)

        save()
        account.isAuthenticatedSubject.onNext(isAuthenticated())
    }

    private fun setVariables(user: User?, registrationState: RegistrationState?, token: String?) {
        if (user == null || token == null || registrationState == null) {
            if (!(user == null && token == null && registrationState == null)) {
                account.errors.unexpected("Inconsistent registration state, missing token")
            }
            this.user = null
            this.token = null
            this.registrationState = null
        } else {
            this.user = user
            this.token = token
            this.registrationState = registrationState
        }
    }


    fun save() {
        account.storage.save("oh.auth.user", user)
        account.storage.save("oh.auth.registrationState", registrationState)
        account.storage.save("oh.auth.token", token)
    }

    fun load() {
        val user = account.storage.load("oh.auth.user", object : TypeReference<User>() {})
        val registrationState =
            account.storage.load("oh.auth.registrationState", object : TypeReference<RegistrationState>() {})
        val token = account.storage.load("oh.auth.token", object : TypeReference<String>() {})
        setVariables(user, registrationState, token)
    }

    fun updateUser(newUser: User) {
        if (newUser.id != user?.id) {
            throw HitchApiError("Updating user with wrong user id. expected: ${user?.id}, given:${newUser.id}")
        }
        user = newUser
        account.storage.save("oh.auth.user", newUser)
    }

    fun updateRegistrationState(newRegistrationState: RegistrationState) {
        registrationState = newRegistrationState
        account.storage.save("oh.auth.registrationState", newRegistrationState)
    }

    fun isAuthenticated() = token != null

    fun isFullyRegistered() = true == registrationState?.fully_registered
}
