package de.wifaz.oh.api

import com.mapbox.geojson.Point
import de.wifaz.oh.protocol.OHPoint

fun OHPoint.toPoint(): Point {
    return Point.fromLngLat(longitude, latitude)
}

