package de.wifaz.oh.api

import com.mapbox.api.directions.v5.DirectionsCriteria
import java.util.*

class Settings {
    var locale: Locale = Locale("en")
    var unitType: String = DirectionsCriteria.METRIC
    lateinit var navigateApiUrl: String
}