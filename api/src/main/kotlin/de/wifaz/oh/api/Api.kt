package de.wifaz.oh.api

import com.mapbox.api.directions.v5.DirectionsCriteria
import com.mapbox.api.directions.v5.MapboxDirections
import com.mapbox.api.directions.v5.models.DirectionsResponse
import com.mapbox.api.directions.v5.models.DirectionsRoute
import de.wifaz.oh.api.Errors.Type.*
import de.wifaz.oh.app.service.Alerts
import de.wifaz.oh.app.service.Ways
import de.wifaz.oh.app.service.reference_way_id
import de.wifaz.oh.protocol.*
import de.wifaz.oh.protocol.Feedback
import io.reactivex.Observable
import io.reactivex.Observable.just
import io.reactivex.subjects.ReplaySubject
import okhttp3.ResponseBody
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response
import java.io.File
import java.net.ConnectException
import java.util.*

open class HitchApiReferenceException(message: String) : RuntimeException(message)
open class HitchApiError(message: String) : RuntimeException(message)

val CURRENT_POSITION_MARKER = "\u2295 "

class Api(private val os: Os, val hitchInterface: HitchInterface) {
    val settings = Settings()
    val errors = Errors()
    val callHandler = CallHandler(errors)
    val storage = Storage(os, errors)
    val account = Account(hitchInterface, storage, errors, callHandler)

    val alerts = Alerts()
    val ways = Ways(this)

    fun digest(message: HitchMessage?) {
        try {
            if (message is LiftInfoMessage) {
                ways.putLiftInfos(message.reference_way_id, message.lift_infos)
            } else if (message is LiftStatusMessage) {
                ways.changeLiftStatus(message.reference_way_id, message.lift_id, message.status)
            } else if (message is WayStatusMessage) {
                ways.changeWayStatus(message.way_id, message.status)
            }
        } catch (e:HitchApiReferenceException) {
            // Due to synchronization issues, it happens, that Messages come in for ways, that are already
            // deleted.
            errors.error(NONFATAL,e.message!!)
        }
    }

    fun createFeedback(
        feedback: Feedback,
        onError: () -> Unit = {}, onSuccess: () -> Unit = {}
    ) {
        val call = hitchInterface.createFeedback(feedback)
        callHandler.enqueueCall(
            call,
            String.format("Creating new feedback", feedback.toString(), onError)
        )
        onSuccess()
    }

    fun changeLiftStatus(
        way_id: String, lift_id: String, newStatus: Lift.Status,
        onError: () -> Unit = {}, onSuccess: () -> Unit = {}
    ) {
        val call = hitchInterface.updateLiftStatus(lift_id, newStatus)
        callHandler.enqueueCall(
            call,
            String.format("Uploading new status %s for lift %s", newStatus.name, lift_id),
            onError
        ) {
            ways.changeLiftStatus(way_id, lift_id, newStatus)
            onSuccess()
        }
    }

    fun changeWayStatus(
        way_id: String, newStatus: Way.Status,
        onError: () -> Unit = {}, onSuccess: () -> Unit = {}
    ) {
        val call = hitchInterface.updateWayStatus(way_id, newStatus)
        callHandler.enqueueCall(
            call,
            String.format("Uploading new status %s for way %s", newStatus.name, way_id),
            onError
        ) {
            ways.changeWayStatus(way_id, newStatus)
            onSuccess()
        }
    }

    fun deleteWay(wayId: String, onError: () -> Unit = {}, onSuccess: () -> Unit = {}) {
        val call = hitchInterface.deleteWay(wayId)
        callHandler.enqueueCall(call, String.format("Deleting way %s", wayId), onError) {
            ways.removeWay(wayId)
            File(os.cacheDirectory, wayId).deleteRecursively()
            onSuccess()
        }
    }

    fun createWay(way: Way, onError: () -> Unit = {}, onSuccess: () -> Unit = {}) {
        ways.putWay(way.copy(status = Way.Status.PRELIMINARY))
        val call = hitchInterface.createWay(way)
        callHandler.enqueueCall(call, String.format("Uploading new way %s", way.toString()), onError) {
            ways.changeWayStatus(way.id, way.status)
            onSuccess()
        }
    }

    fun sync() {
        val userId = account.user?.id
        if (userId != null && account.isFullyRegistered()) {
            val call = hitchInterface.getSyncData(userId)
            callHandler.enqueueCall(call, "Getting sync data from server", onError = {}, onSuccess = ::receiveSyncData)
        } else {
            ways.erase()
            alerts.erase()
        }
    }

    /**
     * Feeds data returned by HitchInterface.GetUserData into the local database
     */
    internal fun receiveSyncData(syncData: SyncData) {
        ways.erase()
        alerts.erase()

        account.updateUserData(syncData.user)
        for (way in syncData.ways) {
            ways.putWay(way)
        }
        for (liftInfo in syncData.lift_infos) {
            ways.putLiftInfo(liftInfo.reference_way_id(), liftInfo)
        }
    }

    /**
     * Get the picture of a partner from cache or download it
     *
     * @param wayId: Id of the way on behalf of which the picture is downloaded
     * This serves as primitive scheme of cache invalidation. New ways yield get downloads.
     * The cache is deleted together with the way.
     * @param userId: the id of the user, whose picture is being requested
     *
     * @return Observable, that eventually emits a File obeject that points to
     * a file containing the picture
     */
    // XXX wayId is cache id
    fun fetchPartnerPicture(wayId: String, userId: String): Observable<File> {
        val relativePath = combinePath(wayId, "partnerPictures", userId + ".jpg")
        val call: Call<ResponseBody> = hitchInterface.getProfilePicture(userId)
        return download(relativePath, call)
    }

    private val runningDownloads: MutableMap<String, ReplaySubject<File>> = HashMap()

    /**
     * Download with cache
     *
     * First look into the cache. If the file is already present there, return it.
     *
     * Then look into {@link runningDownloads}, if the download of that file has already been
     * started. If present, return the rx-Subject stored there. {@link Api#onDownloadReady} will
     * eventually emit the File through this subject.
     *
     * Last, start a new download and put a new Subject into the list
     *
     * @param relativePath: Path of the cache file where the download is to be saved.
     * Identical downloads must be mapped to identical paths to make the download mechanism work.
     * @param call: Not yet enqueued Retrofit call to start the download.
     * @return Observable, that delivers the requested file exacly once (or an error)
     */
    private fun download(relativePath: String, call: Call<ResponseBody>): Observable<File> {
        val file = File(os.cacheDirectory, relativePath)
        if (file.exists()) {
            return just(file)
        }
        synchronized(runningDownloads) {
            runningDownloads.get(relativePath)?.let {
                return it
            }
            val subject = ReplaySubject.create<File>()
            runningDownloads.put(relativePath, subject)

            callHandler.enqueueCall(call, "Get profile picture", {}) {
                onDownloadReady(relativePath, it.bytes())
            }
            return subject
        }
    }

    /**
     * @see Api#download
     *
     * @param relativePath: file path relative to cache directory
     * @param bytes: the bytes downloaded
     */
    private fun onDownloadReady(relativePath: String, bytes: ByteArray) {
        val file = File(os.cacheDirectory, relativePath)
        file.parentFile.mkdirs()
        file.createNewFile()
        file.writeBytes(bytes)
        val subject = synchronized(runningDownloads) {
            runningDownloads.remove(relativePath)
        }
        if (subject == null) {
            throw HitchApiError("No downloadSubject, relativePath: $relativePath")
        } else {
            subject.onNext(file)
            subject.onComplete()
        }
    }

    /**
     * Concatenate arguments to build a relative path
     *
     * we cannot use Paths.get() as we support API-level 16 an cannot rely on Java 7
     */
    private fun combinePath(path1: String, vararg paths: String): String {
        var file = File(path1)
        paths.forEach {
            file = File(file, it)
        }
        return file.path
    }

    fun geocodeSearch(
        query: String,
        point: OHPoint,
        onError: () -> Unit,
        onSuccess: (List<OHPoint>) -> Unit
    ) {
        val call: Call<List<OHPoint>> =
            hitchInterface.geocodeSearch(query, point.latitude, point.longitude, settings.locale.language)
        callHandler.enqueueCall(call, "Geographic search", onError, onSuccess)
    }

    fun geocodeReverseSearch(point: OHPoint, onError: () -> Unit, onSuccess: (List<OHPoint>) -> Unit) {
        val call: Call<List<OHPoint>> =
            hitchInterface.geocodeReverseSearch(point.latitude, point.longitude, settings.locale.language)
        callHandler.enqueueCall(call, "Reverse geographic search", onError, onSuccess)
    }

    private data class Event(
        val wayPoint:OHPoint,
        val time:Long,
    )

    /**
     * Plan route to comply with the given lifts
     *
     * As a temporary solution, the pickup and dropoff-points are headed in the order of the
     * pickup/dropoff-time of the individual lifts. This yields reasonable results, if these points
     * are close to the route.
     * Later we will ask the server to solve the travelling salesman problem. Also, the additional deviation times
     * for the lifts that are not yet chosen, have to be updated.
     */
    fun planRoute(way:Way,liftInfos: List<LiftInfo>, onError: () -> Unit, onSuccess: (DirectionsRoute) -> Unit) {
        val events = (liftInfos.map { Event(it.lift.pick_up_point,it.lift.pickup_time) }
                + liftInfos.map { Event(it.lift.drop_off_point,it.lift.drop_off_time) })
        val wayPoints = listOf(way.waypoints[0]) + events.sortedBy { it.time }.map {it.wayPoint} + listOf(way.waypoints.last())
        fetchRoute(wayPoints, onError, onSuccess)
    }

    fun fetchRoute(waypoints: List<OHPoint>, onError: () -> Unit, onSuccess: (DirectionsRoute) -> Unit) {
        val points = waypoints.map { it.toPoint() }

        // FIXME cache known Routes

        val directionsBuilder = MapboxDirections.builder()
            .annotations(DirectionsCriteria.ANNOTATION_CONGESTION, DirectionsCriteria.ANNOTATION_DISTANCE)
            .language(settings.locale)
            .voiceUnits(settings.unitType)
            .profile(DirectionsCriteria.PROFILE_DRIVING)
            .user("gh")
            .accessToken("pk.")  // Our server does not use the access-token, but the Mapbox client demands for it
            .baseUrl(settings.navigateApiUrl)
            .alternatives(false)
            .steps(true)
            .continueStraight(true)
            .geometries(DirectionsCriteria.GEOMETRY_POLYLINE6)
            .overview(DirectionsCriteria.OVERVIEW_FULL)
            .voiceInstructions(true)
            .bannerInstructions(true)
            .roundaboutExits(true)

        directionsBuilder.origin(points[0])
        directionsBuilder.addBearing(null, null)
        for (i in 1 until points.size - 1) {
            directionsBuilder.addWaypoint(points[i])
            directionsBuilder.addBearing(null, null)
        }
        directionsBuilder.destination(points.last())
        directionsBuilder.addBearing(null, null)

        directionsBuilder.build().enqueueCall(object : Callback<DirectionsResponse> {
            override fun onResponse(call: Call<DirectionsResponse>, response: Response<DirectionsResponse>) {
                if (response.isSuccessful) {
                    response.body()?.routes()?.forEach {
                        // We only want the first route (if any)
                        onSuccess(it)
                        return
                    }

                    // The iteration above was not entered, so there is no valid route
                    onError()
                    errors.error(GENERAL, "No valid route returned by server")
                } else {
                    onError()
                    errors.unexpected(
                        "Unsuccessful route planning call",
                        "status: ${response.code()}, message: ${response.message()}"
                    )
                }
            }

            override fun onFailure(call: Call<DirectionsResponse>, throwable: Throwable) {
                if (throwable is ConnectException) {
                    onError()
                    errors.error(CONNECTIVITY, "Server connection failed for route planning call", null, throwable)
                } else {
                    onError()
                    errors.unexpected("Route planning call failed", null, throwable)
                }
            }
        })

    }

}

