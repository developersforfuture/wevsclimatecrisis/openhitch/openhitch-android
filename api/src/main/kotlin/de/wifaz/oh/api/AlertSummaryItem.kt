package de.wifaz.oh.api

import de.wifaz.oh.app.service.Alert

class AlertSummaryItem(
    alert: Alert
) {
    var count: Int = 1
        private set
    var alert: Alert = alert
        private set

    fun add(alert: Alert) {
        this.alert = alert
        count++
    }
}
