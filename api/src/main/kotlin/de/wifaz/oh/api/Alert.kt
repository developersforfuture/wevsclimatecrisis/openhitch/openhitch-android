package de.wifaz.oh.app.service

sealed class Alert {
    abstract fun getKey(): String

    class Request(val way_id: String, val lift_id: String) : Alert() {
        override fun getKey(): String {
            return lift_id
        }
    }

    class Offer(val way_id: String, val lift_id: String) : Alert() {
        override fun getKey(): String {
            return lift_id
        }
    }

    /*
    class Departure(val way_id:String) : Alert() {
            override fun getKey(): Any {
                    return way_id
            }
    }

    class Chat(val user_id:String, val messageId:String) : Alert() {
            override fun getKey(): Any {
                    return Pair(user_id,messageId)
            }
    }
     */
}
