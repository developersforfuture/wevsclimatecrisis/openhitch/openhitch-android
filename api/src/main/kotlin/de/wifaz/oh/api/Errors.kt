package de.wifaz.oh.api

import io.reactivex.Observable
import io.reactivex.subjects.PublishSubject

class Errors {
    enum class Type {
        GENERAL,
        UNEXPECTED,
        CONNECTIVITY,
        AUTHENTICATION,
        NONFATAL,
    }

    data class Error(
        val type: Type,
        val message: String,
        val info: String?,
        val throwable: Throwable?
    )

    class UnexpectedError(message: String) : java.lang.Exception(message)

    private val errorSubject: PublishSubject<Error> = PublishSubject.create()

    fun errorStream(): Observable<Error> = errorSubject

    fun error(type: Type, message: String, info: String? = null, throwable: Throwable? = null) {
        var t = throwable
        if (throwable == null && type == Type.UNEXPECTED) {
            try {
                throw UnexpectedError(message)
            } catch (ex: UnexpectedError) {
                t = ex
            }
        }
        val e = Error(type, message, info, t)
        errorSubject.onNext(e)
    }

    fun unexpected(message: String, info: String? = null, throwable: Throwable? = null) {
        error(Type.UNEXPECTED, message, info, throwable)
    }
}


